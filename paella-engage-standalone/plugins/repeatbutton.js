paella.plugins.RepeatButtonPlugin = Class.create(paella.PlaybackControlPlugin,{
	buttonId:'',
	button:null,

	getRootNode:function(id) {
		this.buttonId = id + '_repeat_button';
		var thisClass = this;
		this.button = new Button(this.buttonId,'repeatButton',function(event) { thisClass.repeatButtonClick() });
		setAriaLabel(this.button.domElement, "Rewind 30 seconds");
        this.button.domElement.setAttribute("tabindex", "20");
		return this.button;
	},

	getWidth:function() {
		return 50;
	},

	setLeftPosition:function(left) {
		this.button.domElement.style.left = left + 'px';
		this.button.domElement.style.position = 'absolute';
	},

	repeatButtonClick:function() {
	    changeVideoPosition(-30);
	},

	checkEnabled:function(onSuccess) {
		onSuccess(true);
	},

	getIndex:function() {
		return 2;
	},

	getName:function() {
		return "RepeatButtonPlugin";
	},

	getMinWindowSize:function() {
		return 750;
	}
});

new paella.plugins.RepeatButtonPlugin();
