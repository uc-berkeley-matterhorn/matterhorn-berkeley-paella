var viewModeFocusClass = 'viewModeThumbnailFocus';

var ProfileItemButton = Class.create(DomNode,{
    viewModePlugin:null,

    initialize:function(icon,profileName,viewModePlugin) {
        this.parent('div',profileName + '_button',{display:'block',backgroundImage:'url(' + icon + ')',width:'78px',height:'41px'});
        this.viewModePlugin = viewModePlugin;
        var thisClass = this;
        this.domElement.setAttribute('aria-live','polite');
        this.domElement.setAttribute('role','button');
        this.domElement.setAttribute('aria-label', profileName.replace('_',' '));
        $(this.domElement).on('focusin mouseenter', function(event) {
            $(event.currentTarget).addClass(viewModeFocusClass);
        });
        $(this.domElement).on('focusout mouseleave', function(event) {
            $(event.currentTarget).removeClass(viewModeFocusClass);
        });
        $(this.domElement).bind('click keyup', function(event) {
            viewModePlugin.viewModePress(event);
        });
    }
});

paella.plugins.ViewModePlugin = Class.create(paella.PlaybackPopUpPlugin,{
    viewModeContainer:'',
    button:'',

    getRootNode:function(id) {
        var thisClass = this;
        this.button = new Button(id + '_view_mode_button','showViewModeButton',function(event) { thisClass.viewModePress(event) },true);
        this.button.domElement.setAttribute('tabindex',55);
        $(document).keyup(function(event) {
            if (thisClass.button.isToggled()) {
                thisClass.keyUp(event);
            }
        });
        setAriaLabel(this.button.domElement, 'Change video layout');
        return this.button;
    },

    getCurrentFocus:function() {
        var current = $('.' + viewModeFocusClass);
        if (current.length == 0) {
            current = $('#' + paellaPlayer.selectedProfile + '_button');
        }
        return current;
    },

    getWidth:function() {
        return 45;
    },

    isContainerVisible:function() {
        return $(this.viewModeContainer.domElement).is(':visible');
    },

    setRightPosition:function(position) {
        this.button.domElement.style.right = position + 'px';
    },

    getPopUpContent:function(id) {
        var thisClass = this;
        this.viewModeContainer = new DomNode('div',id + '_viewmode_container',{display:'none'});
        paella.Profiles.loadProfileList(function(profiles) {
            for (var profile in profiles) {
                var profileData = profiles[profile];
                var imageUrl = 'config/profiles/resources/' + profileData.icon;
                thisClass.viewModeContainer.addNode(new ProfileItemButton(imageUrl,profile,thisClass));
                // Profile icon preload
                var image = new Image();
                image.src = imageUrl;
            }
        });
        return this.viewModeContainer;
    },

    viewModePress:function(event) {
        if (event.type=='click' || event.which==13) {
            if (!this.isContainerVisible()) {
                if (frameControlPlugin && frameControlPlugin.framesControl.isVisible()) {
                    frameControlPlugin.escapePopUp();
                }
                $(this.viewModeContainer.domElement).show();
                this.highlightNewProfileButton(null, paellaPlayer.selectedProfile);
            }
            else {
                var focus = $('.viewModeThumbnailFocus');
                if (focus) {
                    var focusId = focus.attr('id');
                    if (focusId) {
                        var currentProfileName = paellaPlayer.selectedProfile;
                        profileName=focusId.replace('_button','')
                        if (profileName!=currentProfileName) {
                            this.highlightNewProfileButton(currentProfileName, profileName);
                            paellaPlayer.setProfile(profileName);
                        }
                    }
                }
                $(this.viewModeContainer.domElement).hide();
            }
        }
    },

    escapePopUp:function() {
        $(viewModePlugin.viewModeContainer.domElement).hide();
        viewModePlugin.button.toggle();
    },

    isRelevantToCurrentVideo:function() {
        return this.isRecordingWithTwoVideoStreams();
    },

    isRecordingWithTwoVideoStreams:function() {
        var hasPresenter = false;
        var hasPresentation = false;
        var tracks = paella.matterhorn.episode.mediapackage.media.track;
        for (var i=0; i < tracks.length; i++) {
            var type = tracks[i].type;
            if (type.indexOf('presenter') !== -1) {
                hasPresenter = true;
            } else if (type.indexOf('presentation') !== -1) {
                hasPresentation = true;
            }
            if (hasPresenter && hasPresentation) {
                break;
            }
        }
        return hasPresenter && hasPresentation;
    },

    checkEnabled:function(onSuccess) {
        onSuccess(!paella.player.videoContainer.isMonostream);
    },

    getIndex:function() {
        return 101;
    },

    getName:function() {
        return 'ViewModePlugin';
    },

    getMinWindowSize:function() {
        return 500;
    },

    highlightNewProfileButton:function(currentProfileName, newProfileName) {
        $('.' + viewModeFocusClass).removeClass(viewModeFocusClass);
        if (currentProfileName != null) {
            var currentButtonId = currentProfileName + '_button';
            var currentButton = $('#' + currentButtonId);
            $(currentButton).css({'background-position':'0px 0px'});
            $(currentButton).attr('aria-label', currentProfileName.replace('_',' '));
        }
        var newButtonId = newProfileName + '_button';
        var newButton = $('#' + newButtonId);
        $(newButton).css({'background-position':'-78px 0px'});
        $(newButton).attr('aria-label', newProfileName.replace('_',' ') + ' video layout applied');
    }

});

var viewModePlugin = new paella.plugins.ViewModePlugin();
