var frameThumbnail = 'frameThumbnail';
var enabledFrameThumbnail = 'enabledFrameThumbnail';
var focusedFrameThumbnail = 'focusedFrameThumbnail';
var imagePreviewId = 'preview';

var FrameThumbnail = Class.create(DomNode,{
	isCurrentFrame:null,
	frameData:null,
	nextFrameData:null,
	frameContainer:null,

	initialize:function(frameData,frameContainer) {
		paella.debug.log('create frame thumbnail');
		this.parent('img',frameData.id,{float:'left'});
		this.domElement.className = frameThumbnail;
		this.domElement.setAttribute('src',frameData.url);
		this.domElement.setAttribute('height','40');
		this.domElement.setAttribute('width','60');
        this.domElement.setAttribute('role', 'button');
        this.domElement.setAttribute('aria-live', 'polite');
        this.domElement.setAttribute('aria-label', frameData.time + ' seconds');
		this.frameContainer = frameContainer;
		this.frameData = frameData;
		var thisClass = this;
		$(document).bind(paella.events.setTrim,function(event,params) {
			thisClass.checkVisibility(params.trimEnabled,params.trimStart,params.trimEnd);
		});
		$(this.domElement).bind('focusin mouseenter', function(event) {
            $(event.currentTarget).addClass(focusedFrameThumbnail);
            var previewImage = thisClass.frameContainer.frameHiRes[thisClass.frameData.time];
            if (previewImage === undefined){
                previewImage = thisClass.frameContainer.frames[thisClass.frameData.time];
            }
            if (previewImage != undefined){
                var x = event.currentTarget.x;
                var positionX = ($(window).width() / 2) > x ? x : x - 330;
                var y = event.currentTarget.y;
                y = (y <= 200) ? $(event.currentTarget).offset().top : y;
                floatImagePreview(imagePreviewId, previewImage.url, positionX, y - 200);
            }
        });
		$(this.domElement).bind('focusout mouseleave', function(event) {
            $('.' + focusedFrameThumbnail).removeClass(focusedFrameThumbnail);
            $('#' + imagePreviewId).remove();
        });
        $(this.domElement).bind('click keypress', function(event) {
            frameControlPlugin.showFramesPress(event);
        });
	},

	setNextFrameData:function(nextFrameData) {
		this.nextFrameData = nextFrameData;
	},

	setCurrent:function(current) {
		this.isCurrentFrame = current;
		if (current) {
            $('.' + focusedFrameThumbnail).removeClass(focusedFrameThumbnail);
			this.domElement.className = enabledFrameThumbnail;
			$(this.domElement).addClass(focusedFrameThumbnail)
		}
		else {
			this.domElement.className = frameThumbnail;
		}
	},

	checkVisibility:function(trimEnabled,trimStart,trimEnd) {
		if (!trimEnabled) {
			$(this.domElement).show();
		}
		else if (this.frameData.time<trimStart) {
			if (this.nextFrameData && this.nextFrameData.time>trimStart) {
				$(this.domElement).show();
			}
			else {
				$(this.domElement).hide();
			}
		}
		else if (this.frameData.time>trimEnd) {
			$(this.domElement).hide();
		}
		else {
			$(this.domElement).show();
		}
	}
});

var FramesControl = Class.create(DomNode,{
    frames:{},
    frameHiRes:{},
    currentFrame:null,
	hiResFrame:null,

	initialize:function(id) {
		this.parent('div',id,{position:'absolute',left:'0px',right:'0px',bottom:'37px',display:'block'});
		this.domElement.className = 'frameListContainer';
        this.domElement.setAttribute('aria-live', 'polite');
		this.hide();
		var thisClass = this;
		$(document).bind(paella.events.loadComplete,function(event,params) {
			thisClass.setFrames(params.frames);
			thisClass.obtainHiResFrames();
		});
		$(document).bind(paella.events.timeupdate,function(event,params) { thisClass.onTimeUpdate(params) });
		$(document).bind(paella.events.controlBarWillHide,function(event) { thisClass.hide(); });
	},

	seekToTime:function(time) {
		$(document).trigger(paella.events.seekToTime,{time:time});
	},

	isVisible:function() {
		return $(this.domElement).is(':visible');
	},

	show:function() {
		$(this.domElement).show();
	},

	hide:function() {
		$(this.domElement).hide();
	},

	setFrames:function(frames) {
		this.frames = frames;
		var previousFrame = null;
		for(var frame in frames) {
			var frameThumbnail = new FrameThumbnail(frames[frame],this);
			setAriaLabel(frameThumbnail.domElement, describeSeconds(frame));
			frameThumbnail.domElement.setAttribute('time',frame);
			this.addNode(frameThumbnail);
			frameThumbnail.setNextFrameData(previousFrame);
			previousFrame = frame;
		}
	},

	highlightCurrentFrame:function(currentTime) {
		var frame = this.getCurrentFrame(currentTime);
		if (this.currentFrame!=frame) {
			if (this.currentFrame!=null) {
				var lastFrame = this.getNode(this.currentFrame.id);
				lastFrame.setCurrent(false);
			}
			this.currentFrame = frame;
			var frameThumbnail = this.getNode(this.currentFrame.id);
			frameThumbnail.setCurrent(true);
		}
	},

	getCurrentFrame:function(currentTime) {
		return this.frames[this.getCurrentFrameIndex(currentTime)];
	},

	getCurrentFrameIndex:function(currentTime) {
        var currentFrameIndex = 0;
		for (var i=0;i<currentTime;++i) {
			if (this.frames[i]) {
				currentFrameIndex = this.frames[i].time;
			}
		}
		return currentFrameIndex;
	},

	onTimeUpdate:function(memo) {
		if (this.isVisible()) {
			this.highlightCurrentFrame(memo.currentTime);
		}
	},

	obtainHiResFrames:function() {
		var unorderedFrames = {}
		var attachments = paella.matterhorn.episode.mediapackage.attachments.attachment;
		var lastFrameTime = 0;
		for (var attachment in attachments) {
			attachment = attachments[attachment];
			if (attachment.type=='presentation/segment+preview') {
				var url = attachment.url;
				var time = 0;
				if (/time=T(\d+):(\d+):(\d+)/.test(attachment.ref)) {
					time = parseInt(RegExp.$1)*60*60 + parseInt(RegExp.$2)*60 + parseInt(RegExp.$3);
				}
				unorderedFrames[time] = {time:time,url:url,mimetype:attachment.mimetype,id:attachment.id};
				if (time>lastFrameTime) lastFrameTime = time;
			}
		}

		for (var i=0;i<=lastFrameTime;++i) {
			if (unorderedFrames[i]) {
				this.frameHiRes[i] = unorderedFrames[i];
			}
		}
	}
});

paella.plugins.FrameControlPlugin = Class.create(paella.PlaybackPopUpPlugin,{
	framesControl:null,
	container:null,
	button:null,

	getTabName:function() {
		return 'Diapositivas';
	},

	getRootNode:function(id) {
        $(document).keyup(function(event) {
            if (thisClass.framesControl.isVisible()) {
                thisClass.keyUp(event);
            }
        });
		var thisClass = this;
		this.container = new DomNode('div',id + '_frameControl_container');
		this.button = this.container.addNode(new Button(id + '_frameControl_button','showFramesButton',function(event) { thisClass.showFramesPress(event); },true));
        this.button.domElement.setAttribute('tabindex', 60);
        setAriaLabel(this.button.domElement, 'Navigate by slides');
		return this.container;
	},

    isRelevantToCurrentVideo:function() {
        return this.isEpisodeWithSegments();
    },

    isEpisodeWithSegments:function() {
        var segmentCount = 0;
        var attachments = paella.matterhorn.episode.mediapackage.attachments.attachment;
        for (var i=0; i < attachments.length; i++) {
            if (attachments[i].type.indexOf('segment') > -1) {
                segmentCount++;
            }
        }
        return (segmentCount > 0);
    },

	getWidth:function() {
		return 45;
	},

	setRightPosition:function(position) {
		this.button.domElement.style.right = position + 'px';
	},

	getPopUpContent:function(id) {
		var thisClass = this;
		this.framesControl = new FramesControl(id + '_frameContol_frames');
		$(document).bind(paella.events.seekToTime,function(event){
			if (thisClass.showFramesButton().isToggled()) {
				thisClass.showFramesButton().toggleIcon();
			}
		});
		return this.framesControl;
	},

    escapePopUp:function() {
        $('#' + imagePreviewId).remove();
		this.framesControl.hide();
        this.button.toggle();
    },

	showFramesButton:function() {
		return this.button;
	},

	showFramesPress:function(event) {
		if (event.type=="click" || event.which==13) {
			if (this.framesControl.isVisible()) {
                $('#' + imagePreviewId).remove();
                var frame = event.type=="click"? $(event.target): $('.focusedFrameThumbnail');
                if (frame && frame.attr('time')) {
                    var duration = paella.player.videoContainer.duration();
                    // We add one (1) to the time to make sure we don't land on previous slide.
                    var time = parseInt(frame.attr('time')) + 1;
                    changeVideoPositionPercentage(time * 100 / duration);
                    frame.attr('aria-label', 'Skip to '+~~(time/60)+' minutes and '+(time % 60)+' seconds');
                    this.framesControl.highlightCurrentFrame(time);
                    this.framesControl.seekToTime(time);
                }
				this.framesControl.hide();
	            setAriaLabel(this.button.domElement, 'Hide slides');
			}
			else {
                if (viewModePlugin && viewModePlugin.isContainerVisible()) {
                    viewModePlugin.escapePopUp();
                }
				this.framesControl.show();
				var hasActiveFrame = this.framesControl && this.framesControl.currentFrame != null;
				var time = hasActiveFrame ? this.framesControl.currentFrame.time : 0;
				this.framesControl.highlightCurrentFrame(time);
	            setAriaLabel(this.button.domElement, 'Show slides');
			}
		}
	},

    getCurrentFocus:function() {
        var current = $('.' + focusedFrameThumbnail);
        if (current.length == 0) {
            current = $('.' + frameThumbnail)[0];
        }
        return $(current);
    },

	checkEnabled:function(onSuccess) {
	    onSuccess(true);
	},

	getIndex:function() {
		return 100;
	},

	getName:function() {
		return 'FrameControlPlugin';
	},

	getMinWindowSize:function() {
		return 600;
	}

});

var frameControlPlugin = new paella.plugins.FrameControlPlugin();
