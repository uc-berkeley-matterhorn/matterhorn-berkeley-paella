#!/bin/bash

# Build MediaElement.js package
mepHome=${PAELLA_HOME}/paella-engage-standalone/mediaelement-js-source

buildDir=${mepHome}/build
mkdir -p ${buildDir}
cd ${mepHome}/src
python Builder.py

# Remove unwanted
rm -f ${buildDir}/DO\ NOT\ CHANGE\ THESE\ FILES.\ USE\ -src-\ FOLDER.txt \
    ${buildDir}/jquery.js \
    ${buildDir}/*.svg

# Deploy
targetDir=${PAELLA_HOME}/paella-engage-standalone/resources/mep
rm -Rf ${targetDir}
mkdir -p ${targetDir}
cp -R ${buildDir}/* ${targetDir}

# Clean up
rm -Rf ${buildDir}

exit 0
