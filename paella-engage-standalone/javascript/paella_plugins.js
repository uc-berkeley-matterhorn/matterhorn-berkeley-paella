paella.plugins.captions = {classes:{}, instances:{}, events:{}, captions:null, enableEdit:false};

paella.plugins.captions.events = {
	loaded:'captions:loaded',
	enable:'captions:enable',
	disable:'captions:disable'
};

function isEpisodeWithCaptions() {
    var hasCaptions = false;
    var catalogArray = paella.matterhorn.episode.mediapackage.metadata.catalog;
    for (var i=0; i < catalogArray.length; i++) {
        if (catalogArray[i].type.indexOf('captions') !== -1) {
            hasCaptions = true;
            break;
        }
    }
    return hasCaptions;
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Captions Loader
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
paella.plugins.captions.classes.DFXPParser = Class.create({

	parseCaptions:function(text)
	{
		var xml = $(text);
		var ps = xml.find("body div p");
		var captions= [];
		var i = 0;
		for (i=0; i< ps.length; i++) {
			var c = this.getCaptionInfo(ps[i]);
			captions.push(c);
		}
		return captions;
	},

	getCaptionInfo:function(cap) {
		var b = this.parseTimeTextToSeg(cap.getAttribute("begin"));
		var d = this.parseTimeTextToSeg(cap.getAttribute("end"));
		var v = $(cap).text();

		return {begin:b, duration:d, value:v};
	},

	parseTimeTextToSeg:function(ttime){
		var split = ttime.split(":");
		var h = parseInt(split[0]);
		var m = parseInt(split[1]);
		var s = parseInt(split[2]);
		return s+(m*60)+(h*60*60);
	},

	captionsToDxfp:function(captions){
		var xml = '<?xml version="1.0" encoding="UTF-8"?>\n';
		xml = xml + '<tt xml:lang="en" xmlns="http://www.w3.org/2006/10/ttaf1" xmlns:tts="http://www.w3.org/2006/04/ttaf1#styling">\n';
		xml = xml + '<body><div xml:id="captions" xml:lang="en">\n';

		for (var i=0; i<captions.length; i=i+1){
			var c = captions[i];
			xml = xml + '<p begin="'+ paella.utils.timeParse.secondsToTime(c.begin) +'" end="'+ paella.utils.timeParse.secondsToTime(c.duration) +'">' + c.value + '</p>\n';
		}
		xml = xml + '</div></body></tt>';

		return xml;
	}
});


paella.plugins.captions.classes.CaptionsLoader = Class.create({

	initialize:function() {
		var thisClass = this;
		$(document).bind(paella.events.loadComplete,function(event,params) {
			if ((paella.player.config.captions) && (paella.player.config.captions.enableEdit)){
				paella.plugins.captions.enableEdit = true;
			}
			thisClass.loadCaptions();
		});
	},

	loadCaptionsUsingMediapackage:function(onSuccess, onError){
		var catalogs = null;
		try {
			catalogs = paella.matterhorn.episode.mediapackage.metadata.catalog;
			if (!(catalogs instanceof Array)){
			    catalogs = [catalogs];
			}
		}
		catch(e){catalogs = null;}
		if (catalogs != null){
			var catalog = null;
			for (var i=0; i< catalogs.length; i=i+1){
				var c = catalogs[i];
				if (c.type == "captions/timedtext"){
					catalog = c;
					break;
				}
			}
			if (catalog != null){
				paella.debug.log("Captions found in MediaPackage: Loading Captions file...");
				// Load captions!
				var proxyUrl = '';
				var useJsonp = paella.player.config.proxyLoader.usejsonp;
				if (paella.player.config.proxyLoader && paella.player.config.proxyLoader.enabled) {
					proxyUrl = paella.player.config.proxyLoader.url;
				}

				new paella.Ajax(catalog.url, {}, function(response) {
					if (response){
						var parser = new paella.plugins.captions.classes.DFXPParser();
						paella.plugins.captions.captions = parser.parseCaptions(response);
						$(document).trigger(paella.plugins.captions.events.loaded, {});
						if (onSuccess) onSuccess();
					}
					else{
						if (onError) onError();
					}
				}, proxyUrl, useJsonp, 'GET');

			}
			else{
				paella.debug.log("Captions does not found in MediaPackage!");
			}
		}
	},

	loadCaptionsUsingAnnotations:function(onSuccess, onError){
		var episodeid = paella.matterhorn.episode.id;
		this.loadAttachmentData(episodeid, "paella/captions/timedtext", function(value){
			if (value){
				paella.debug.log("Captions found in the annotation service: Loading annotation file...");
				var parser = new paella.plugins.captions.classes.DFXPParser();
				paella.plugins.captions.captions = parser.parseCaptions(value);
				$(document).trigger(paella.plugins.captions.events.loaded, {});
				if (onSuccess) onSuccess();
			}
			else{
				paella.debug.log("Captions does not found in the annotation service!");
				if (onError) onError();
			}
		}, onError);
	},


	loadCaptions:function(onSuccess, onError){
		// Try to load Captions from annotation service first if active....
		var thisClass = this;
		if (this.enableEdit){
			this.loadCaptionsUsingAnnotations(null, function(){ thisClass.loadCaptionsUsingMediapackage(); });
		}
		else{
			this.loadCaptionsUsingMediapackage();
		}
	},

	saveCaptions:function(onSuccess, onError){
		var episodeid = paella.matterhorn.episode.id;
		var value = parser.captionsToDxfp(paella.plugins.captions.captions);
		paella.debug.log("Saving captions in the annotation service: Loading Annotation file...");

		this.saveAttachmentData(episodeid, "paella/captions/timedtext", value, onSuccess, onError);
	},

	loadAttachmentData:function(episodeid, type, onSuccess, onError){
		var loader = new paella.matterhorn.LoaderSaverInfo(paella.player.config);
		loader.loadData(episodeid, type, onSuccess, onError);
	},

	saveAttachmentData:function(episodeid, type, value, onSuccess, onError){
		var saver = new paella.matterhorn.LoaderSaverInfo(paella.player.config);
		saver.saveData(episodeid, type, value, onSuccess, onError);
	}

});

paella.plugins.captions.instances.captionsLoader = new paella.plugins.captions.classes.CaptionsLoader();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Captions Player Button Plugin
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
paella.plugins.captions.classes.CaptionsPlayerButtonPlugin = Class.create(paella.PlaybackPopUpPlugin,{
	button:null,
	thereAreCaptions:false,

	initialize:function() {
		this.parent();
		var thisClass = this;
		this.button = new Button('loading_captionsplayer_button','captionsButton_noCaptions',function(event) { thisClass.onButtonClick(); }, true);
        this.button.domElement.setAttribute("tabindex", "40");
        setAriaLabel(this.button.domElement, "Show captions");
		$(document).bind(paella.plugins.captions.events.loaded,function(event,params) {
            thisClass.thereAreCaptions = true;
            thisClass.button.domElement.className = "captionsButton";
		});
	},

	isRelevantToCurrentVideo:function() {
		return isEpisodeWithCaptions();
	},

	getRootNode:function(id) {
		this.button.identifier = id + '_captionsplayer_button';
		this.button.domElement.id = this.button.identifier;
		return this.button;
	},

	getWidth:function() {
		return 45;
	},

	setRightPosition:function(position) {
		this.button.domElement.style.right = position + 'px';
	},

	getPopUpContent:function(id) {
		return null;
	},

	checkEnabled:function(onSuccess) {
		onSuccess(true);
	},

	getIndex:function() {
		return 1005;
	},

	getName:function() {
		return "CaptionsPlayerButtonPlugin";
	},

	getMinWindowSize:function() {
		return 700;
	},

	onButtonClick:function() {
		if (this.thereAreCaptions == true) {
			if (this.button.isToggled()) {
			    setAriaLabel(this.button.domElement, "Show captions");
				$(document).trigger(paella.plugins.captions.events.enable, {});
			}
			else {
			    setAriaLabel(this.button.domElement, "Hide captions");
				$(document).trigger(paella.plugins.captions.events.disable, {});
			}
		}
	}
});

paella.plugins.captions.instances.captionsPlayerButtonPlugin = new paella.plugins.captions.classes.CaptionsPlayerButtonPlugin();



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Captions Player Overlay Plugin
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
paella.plugins.captions.classes.CaptionsPlayerOverlayPlugin = Class.create(paella.EventDrivenPlugin,{
	timer:null,
	visible:false,
	overlayFrame:null,

	getEvents:function() {

		return [paella.events.loadComplete,
			paella.events.play,
			paella.events.pause,
			paella.plugins.captions.events.enable,
			paella.plugins.captions.events.disable
		];

		return [];
	},

	onEvent:function(eventType,params) {
		switch (eventType) {
			case paella.events.loadComplete:
				this.loadComplete();
				break;
			case paella.events.play:
				this.startTimer();
				break;
			case paella.events.pause:
				this.pauseTimer();
				break;
			case paella.plugins.captions.events.enable:
				this.onEnable();
				break;
			case paella.plugins.captions.events.disable:
				this.onDisable();
				break;
		}
	},

	loadComplete:function() {
		var overlayContainer = paella.player.videoContainer.overlayContainer;
		this.overlayFrame = document.createElement("div");
		this.overlayFrame.setAttribute('class',"CaptionsPlayerOverlayPlugin");
		overlayContainer.addElement(this.overlayFrame, overlayContainer.getMasterRect());
	},

	startTimer:function() {
		var thisClass = this;
		this.timer = new paella.utils.Timer(function(timer) {
			thisClass.onUpdateCaptions();
			},1000.0);
		this.timer.repeat = true;
	},

	pauseTimer:function() {
		if (this.timer!=null) {
			this.timer.cancel();
			this.timer = null;
		}
	},

	onUpdateCaptions:function() {
		var captions = paella.plugins.captions.captions;

		if (captions){
			var time = paella.player.videoContainer.currentTime();
			cap = "";
			var i;
			for (i=0; i<captions.length;i++){
				if ((captions[i].begin<=time) && ((captions[i].begin+captions[i].duration)>=time)){
					cap = captions[i].value
				}
			}
			this.overlayFrame.innerHTML = cap;
		}
	},

	onEnable:function() {
        this.visible = true;
        $(this.overlayFrame).show();
	},

	onDisable:function() {
		this.visible = false;
		$(this.overlayFrame).hide();
	},


	checkEnabled:function(onSuccess) {
		onSuccess(true);
	},

	getIndex:function() {
		return 1000;
	},

	getName:function() {
		return "CaptionsPlayerOverlayPlugin";
	}
});

paella.plugins.captions.instances.captionsPlayerOverlayPlugin = new paella.plugins.captions.classes.CaptionsPlayerOverlayPlugin();



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Captions Editor Plugin
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
paella.plugins.captions.classes.CaptionsEditorPlugin = Class.create(paella.editor.TrackPlugin,{
	tracks:[],
	selectedTrackItem:null,

	initialize:function() {
		this.parent();
		var thisClass = this;
		if (paella.utils.language()=="es") {
			var esDict = {
				'Captions':'Subtítulos',
				'Show':'Mostrar',
				'Hide':'Ocultar',
				'Show captions':'Mostrar subtítulos',
				'Hide captions':'Ocultar subtítulos'
			};
			paella.dictionary.addDictionary(esDict);
		}

		$(document).bind(paella.plugins.captions.events.loaded,function(event,params) {
			for (var i =0; i<paella.plugins.captions.captions.length; i=i+1){
				var c = paella.plugins.captions.captions[i];
				var id = thisClass.getTrackUniqueId();
				thisClass.tracks.push({id:id,s:c.begin,e:c.duration,content:c.value, lock:true});
			}
		});

	},

	getTrackItems:function() {
		for (var i=0;i<this.tracks.length;++i) {
			this.tracks[i].name = this.tracks[i].content;
		}
		return this.tracks;
	},

	getTools:function() {
		var tools = [
			{name:'show',label:paella.dictionary.translate('Show'),hint:paella.dictionary.translate('Show captions')},
			{name:'hide',label:paella.dictionary.translate('Hide'),hint:paella.dictionary.translate('Hide captions')}
		];
		if (paella.plugins.captions.enableEdit == true){
			tools.push({name:'create',label:paella.dictionary.translate('Create'),hint:paella.dictionary.translate('Create a new caption in the current position')});
			tools.push({name:'delete',label:paella.dictionary.translate('Delete'),hint:paella.dictionary.translate('Delete selected caption')});
		}

		return tools;
	},

	getTrackItemIndex:function(item) {
		for(var i=0;i<this.tracks.length;++i) {
			if (item.id==this.tracks[i].id) {
				return i;
			}
		}
		return -1;
	},

	onToolSelected:function(toolName) {
		if (this.selectedTrackItem && toolName=='delete' && this.selectedTrackItem) {
			this.tracks.splice(this.getTrackItemIndex(this.selectedTrackItem),1);
			return true;
		}
		else if (toolName=='show') {
			$(document).trigger(paella.plugins.captions.events.enable, {});
		}
		else if (toolName=='hide') {
			$(document).trigger(paella.plugins.captions.events.disable, {});
		}
		else if (toolName=='create') {
			var start = paella.player.videoContainer.currentTime();
			var end = start + 5;
			var id = this.getTrackUniqueId();
			this.tracks.push({id:id,s:start,e:end,content:paella.dictionary.translate('Caption')});
			return true;
		}
	},

	getTrackUniqueId:function() {
		var newId = -1;
		if (this.tracks.length==0) return 1;
		for (var i=0;i<this.tracks.length;++i) {
			if (newId<=this.tracks[i].id) {
				newId = this.tracks[i].id + 1;
			}
		}
		return newId;
	},

	getName:function() {
		return "CaptionsEditorPlugin";
	},

	getTrackName:function() {
		return paella.dictionary.translate("Captions");
	},

	getColor:function() {
		return 'rgb(212, 212, 224)';
	},

	getTextColor:function() {
		return 'rgb(90,90,90)';
	},

	onTrackChanged:function(id,start,end) {
		var item = this.getTrackItem(id);
		if (item) {
			item.s = start;
			item.e = end;
			this.selectedTrackItem = item;
		}
	},

	onTrackContentChanged:function(id,content) {
		var item = this.getTrackItem(id);
		if (item) {
			item.content = content;
			item.name = content;
		}
	},

	allowEditContent:function() {
		return paella.plugins.captions.enableEdit;
	},

	getTrackItem:function(id) {
		for (var i=0;i<this.tracks.length;++i) {
			if (this.tracks[i].id==id) return this.tracks[i];
		}
	},

	contextHelpString:function() {
		if (paella.utils.language()=="es") {
			return "Utiliza esta herramienta para crear, borrar y editar subtítulos. Para crear un subtítulo, selecciona el instante de tiempo haciendo clic en el fondo de la línea de tiempo, y pulsa el botón 'Crear'. Utiliza esta pestaña para editar el texto de los subtítulos";
		}
		else {
			return "Use this tool to create, delete and edit video captions. To create a caption, select the time instant clicking the timeline's background and press 'create' button. Use this tab to edit the caption text.";
		}
	}
});

paella.plugins.captions.instances.captionsEditorPlugin = new paella.plugins.captions.classes.CaptionsEditorPlugin();


paella.plugins.CommentsPlugin  = Class.create(paella.TabBarPlugin,{
	id:null,
	divRoot:null,
	divPublishComment:null,
	divComments:null,
	divLoading:null,
	isPublishAllowed: true,
	isPublishByAnonymousAllowed: false,
	publishCommentTextArea:null,
	publishCommentButtons:null,
	canPublishAComment: false,
	btnAddCommentToInstant: null,
	currentTime: 0,
	proxyUrl:'',
	useJsonp:false,
	commentsTree: [],
	

	initialize:function() {
		this.parent();
		var thisClass = this;

		this.id = 'CommentPlugin';
		this.divRoot = new DomNode('div',this.id ,{display:'block'});

		this.divPublishComment = new DomNode('div','CommentPlugin_Publish' ,{display:'block'});
		this.divLoading = new DomNode('div','CommentPlugin_Loading' ,{display:'none'});		
		this.divComments = new DomNode('div','CommentPlugin_Comments' ,{display:'none'});

		this.divPublishComment.domElement.id = this.id+"_Publish";
		this.divLoading.domElement.id = this.id+"_Loading";
		this.divComments.domElement.id = this.id+"_Comments";
		
		this.divRoot.addNode(this.divPublishComment);
		this.divRoot.addNode(this.divLoading);
		this.divRoot.addNode(this.divComments);


		$(document).bind(paella.events.loadComplete,function(event,params) {
			thisClass.reloadComments();
			
			thisClass.isPublishByAnonymousAllowed = paella.player.config.comments.anonymousCanComment;
	
			if ( ((paella.matterhorn.me.username == "anonymous") && (thisClass.isPublishByAnonymousAllowed == true)) || (paella.matterhorn.me.username != "anonymous") ){
				if (thisClass.isPublishAllowed == true){
					thisClass.canPublishAComment = true;
					thisClass.createPublishComment();
					$(document).bind(paella.events.timeUpdate, function(event, params){
						thisClass.currentTime = params.currentTime;
						var currentTime = params.currentTime;
						if (paella.player.videoContainer.trimEnabled()){
						  currentTime = params.currentTime - paella.player.videoContainer.trimming.start;
						}
						thisClass.btnAddCommentToInstant.domElement.innerHTML = paella.dictionary.translate("Publish at {0}").replace(/\{0\}/g, paella.utils.timeParse.secondsToTime(currentTime));					
					});
				}
			}
		});
	},	
	
	getRootNode:function(id) {
		var thisClass = this;
		this.useJsonp = paella.player.config.proxyLoader.usejsonp;
		if (paella.player.config.proxyLoader && paella.player.config.proxyLoader.enabled) {
			this.proxyUrl = paella.player.config.proxyLoader.url;
		}		
	
		return this.divRoot;
	},

	checkEnabled:function(onSuccess) {
		var enabled = false;
		try {
			enabled = paella.player.config.comments.enabled;
			if (enabled == true){
				var mattEn = paella.matterhorn.me.org.properties["engageui.annotations.enable"];
				if (mattEn != undefined) {
					enabled = (paella.matterhorn.me.org.properties["engageui.annotations.enable"]=="true");
				}
			}
		}
		catch(e) {enabled = false;}

		onSuccess(enabled);
	},	
		

	getIndex:function() {
		return 100;
	},
	
	getTabName:function() {
		return paella.dictionary.translate("Comments");
	},
		
	setLoadingComments:function(b) {
		if ((this.divLoading) && (this.divComments)){
		if (b == true){
			this.divLoading.domElement.style.display="block";
			this.divComments.domElement.style.display="none";
		}
		else{
			this.divLoading.domElement.style.display="none";
			this.divComments.domElement.style.display="block";
		}
		}
	},
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Publish functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	createPublishComment:function() {
		var thisClass = this;
		var rootID = this.divPublishComment.identifier+"_entry";
		var divEntry = new DomNode('div', rootID, {display:'block'});
		divEntry.domElement.className="comments_entry";
		
		var divSilhouette = new DomNode('img',rootID+"_silhouette" ,{display:'inline-block'});
		divSilhouette.domElement.src="plugins/silhouette32.png";
		divSilhouette.domElement.className = "comments_entry_silhouette";
		divEntry.addNode(divSilhouette);
		
		var divTextAreaContainer = new DomNode('div',rootID+"_textarea_container" ,{display:'inline-block'});
		divTextAreaContainer.domElement.className = "comments_entry_container";
		divTextAreaContainer.domElement.onclick = function(){thisClass.onClickTextAreaContainer(divTextAreaContainer)};		
		divEntry.addNode(divTextAreaContainer);
		
		this.publishCommentTextArea = new DomNode('textarea',rootID+"_textarea" ,{display:'block'});
		divTextAreaContainer.addNode(this.publishCommentTextArea);

		this.publishCommentButtons = new DomNode('div',rootID+"_buttons_area" ,{display:'none'});
		divTextAreaContainer.domElement.className = "comments_entry_container";
		divTextAreaContainer.addNode(this.publishCommentButtons);



		var btnAddComment = new DomNode('button',rootID+"_btnAddComment" ,{display:'float', float:'right'});
		btnAddComment.domElement.onclick = function(){thisClass.addComment();};
		btnAddComment.domElement.innerHTML = paella.dictionary.translate("Publish");

		this.publishCommentButtons.addNode(btnAddComment);
		
		this.btnAddCommentToInstant = new DomNode('button',rootID+"_btnAddCommentAt" ,{display:'float', float:'right'});
		
		this.btnAddCommentToInstant.domElement.innerHTML = paella.dictionary.translate("Publish at {0}").replace(/\{0\}/g,'??:??:??');
		this.btnAddCommentToInstant.domElement.onclick = function(){thisClass.addCommentAtTime();};
		this.publishCommentButtons.addNode(this.btnAddCommentToInstant);
		
		divTextAreaContainer.domElement.commentsTextArea = this.publishCommentTextArea;
		divTextAreaContainer.domElement.commentsBtnAddComment = btnAddComment;
		divTextAreaContainer.domElement.commentsBtnAddCommentToInstant = this.btnAddCommentToInstant;
		
				
		this.divPublishComment.addNode(divEntry);
	},
	
	onClickTextAreaContainer:function(textAreaContainerElement){ 
		this.publishCommentTextArea.domElement.style.height="60px";
		this.publishCommentButtons.domElement.style.display="block";
	},
	
	addCommentAtTime:function(){
		var thisClass = this;
		var txtValue = this.publishCommentTextArea.domElement.value;
		txtValue = txtValue.replace(/<>/g, "< >");  //TODO: Hacer este replace bien!
		
		var commentValue = paella.matterhorn.me.username + "<>" + txtValue + "<>scrubber";
		var inTime = Math.floor(thisClass.currentTime);
		
		this.publishCommentTextArea.domElement.value = "";
		
		var restEndpoint = paella.player.config.restServer.url + "annotation"; 		
		new paella.Ajax(restEndpoint,{episode:paella.matterhorn.episode.id, type:"comment", in:inTime, out:0, value:commentValue}, function(response) {
			thisClass.reloadComments();
		}, thisClass.proxyUrl, thisClass.useJsonp, 'PUT'); 	
	},
	
	addComment:function(){
		var thisClass = this;
		var txtValue = this.publishCommentTextArea.domElement.value;
		txtValue = txtValue.replace(/<>/g, "< >");  //TODO: Hacer este replace bien!
		
		var commentValue = paella.matterhorn.me.username + "<>" + txtValue + "<>normal";
	
		this.publishCommentTextArea.domElement.value = "";
		
		var restEndpoint = paella.player.config.restServer.url + "annotation"; 		
		new paella.Ajax(restEndpoint,{episode:paella.matterhorn.episode.id, type:"comment", in:0, out:0, value:commentValue}, function(response) {
			thisClass.reloadComments();
		}, thisClass.proxyUrl, thisClass.useJsonp, 'PUT'); 
	},

	addReply:function(annotationID, domNodeId){
		var thisClass = this;
                
                var textArea = document.getElementById(domNodeId);

		var txtValue = textArea.value;
		txtValue = txtValue.replace(/<>/g, "< >");  //TODO: Hacer este replace bien!
		
		var commentValue = paella.matterhorn.me.username + "<>" + txtValue + "<>reply<>"+annotationID;
	
		textArea.value = "";
		
		var restEndpoint = paella.player.config.restServer.url + "annotation"; 		
		new paella.Ajax(restEndpoint,{episode:paella.matterhorn.episode.id, type:"comment", in:0, out:0, value:commentValue}, function(response) {
			thisClass.reloadComments();
		}, thisClass.proxyUrl, thisClass.useJsonp, 'PUT'); 
	},
		
	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Comments Listing Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	reloadComments:function() {
		var thisClass = this;
		this.useJsonp = paella.player.config.proxyLoader.usejsonp;
		if (paella.player.config.proxyLoader && paella.player.config.proxyLoader.enabled) {
			this.proxyUrl = paella.player.config.proxyLoader.url;
		}
		thisClass.setLoadingComments(true);
		this.divComments.domElement.innerHTML = "";
                thisClass.commentsTree = [];
				
				
		var restEndpoint = paella.player.config.restServer.url + "annotation/annotations.json"; 		
		new paella.Ajax(restEndpoint,{episode:paella.matterhorn.episode.id, type:"comment", limit:1000}, function(response) {
			if (typeof(response)=="string") {
				try {
					response = JSON.parse(response);
				}
				catch(e) {
					response=null;
				}
			}
			if (response  && response.annotations) {
				if (response.annotations.total == 1) {
					response.annotations.annotation = [response.annotations.annotation]
				}
				if (response.annotations.total > 0) {
					response.annotations.annotation.sort(function(a,b){
						var aD = paella.utils.timeParse.matterhornTextDateToDate(a.created);
						var bD = paella.utils.timeParse.matterhornTextDateToDate(b.created);				
						return bD.getTime()-aD.getTime();
					});

                                        var tempDict = {};

                                        // obtain normal and scrubs comments        
					for (var i =0; i < response.annotations.annotation.length; ++i ){
						var annotation = response.annotations.annotation[i];
						
                                                var valuesArray = annotation.value.split("<>");
                                                var valueUser = valuesArray[0];
                                                var valueType = valuesArray[2];
                                                var valueText = valuesArray[1];
                                                valueText = valueText.replace(/\n/g,"<br/>");
                                                
                                                if (valueType !== "reply") { 
                                                  var comment = {};
                                                  comment["id"] = annotation.annotationId;


                                                  comment["user"] = valueUser;
                                                  comment["type"] = valueType;
                                                  comment["text"] = valueText;
                                                  comment["userId"] = annotation.userId;
                                                  comment["created"] = annotation.created;
                                                  comment["inpoint"] = annotation.inpoint;
                                                  comment["replies"] = [];
                                                  
                                                  thisClass.commentsTree.push(comment);
                                                  tempDict[comment["id"]] = thisClass.commentsTree.length - 1; 

                                                }
					}

                                        // obtain replies comments
					for (var i =0; i < response.annotations.annotation.length; ++i ){
						var annotation = response.annotations.annotation[i];

                                                var valuesArray = annotation.value.split("<>");
                                                var valueUser = valuesArray[0];
                                                var valueType = valuesArray[2];
                                                var valueText = valuesArray[1];
                                                var valueParentId = valuesArray[3],
                                                valueText = valueText.replace(/\n/g,"<br/>");
                                                
                                                if (valueType === "reply") { 
                                                  var comment = {};
                                                  comment["id"] = annotation.annotationId;


                                                  comment["user"] = valueUser;
                                                  comment["type"] = valueType;
                                                  comment["text"] = valueText;
                                                  comment["userId"] = annotation.userId;
                                                  comment["created"] = annotation.created;
                                                  
                                                  var index = tempDict[valueParentId];

                                                  thisClass.commentsTree[index]["replies"].push(comment);
                                                }
					}

                                        thisClass.displayComments();
				}
			}
			thisClass.setLoadingComments(false);
			
		}, thisClass.proxyUrl, thisClass.useJsonp);	
		
	},
			
	displayComments:function() {
          var thisClass = this;
          for (var i =0; i < thisClass.commentsTree.length; ++i ){
            var comment = thisClass.commentsTree[i];
            var e = thisClass.createACommentEntry(comment);
            thisClass.divComments.addNode(e);
          } 

        },

	createACommentEntry:function(comment) {
		var thisClass = this;
		var rootID = this.divPublishComment.identifier+"_entry_"+comment["id"];
		
		var divEntry = new DomNode('div',rootID ,{display:'block'});
		divEntry.domElement.className="comments_entry";

		var divSilhouette = new DomNode('img',rootID+"_silhouette" ,{display:'inline-block'});
		divSilhouette.domElement.src="plugins/silhouette32.png";
		divSilhouette.domElement.className = "comments_entry_silhouette";
		divEntry.addNode(divSilhouette);
		
				
		var divCommentContainer = new DomNode('div',rootID+"_comment_container" ,{display:'inline-block'});
		divCommentContainer.domElement.className = "comments_entry_container";
		divEntry.addNode(divCommentContainer);
				

		var divCommentMetadata = new DomNode('div',rootID+"_comment_metadata" ,{display:'block'});
		divCommentContainer.addNode(divCommentMetadata);
		var datePublish = "";
		if (comment["created"]) {
			var dateToday=new Date()
			var dateComment = paella.utils.timeParse.matterhornTextDateToDate(comment["created"]);			
			datePublish = paella.utils.timeParse.secondsToText((dateToday.getTime()-dateComment.getTime())/1000);
		}		
		
		
		var headLine = "<span class='comments_entry_username'>" + comment["userId"] + "</span>";
		if (comment["type"] === "scrubber"){
                        var publishTime = comment["inpoint"];
                        if (paella.player.videoContainer.trimEnabled()){
                            publishTime = comment.inpoint - paella.player.videoContainer.trimming.start;
                        }
			headLine += "<span class='comments_entry_timed'> " + paella.utils.timeParse.secondsToTime(publishTime) + "</span>";
		}
		headLine += "<span class='comments_entry_datepublish'>" + datePublish + "</span>";
 
		divCommentMetadata.domElement.innerHTML = headLine;
		
		var divCommentValue = new DomNode('div',rootID+"_comment_value" ,{display:'block'});
		divCommentValue.domElement.className = "comments_entry_comment";
		divCommentContainer.addNode(divCommentValue);		
		
		divCommentValue.domElement.innerHTML = comment["text"];

		var divCommentReply = new DomNode('div',rootID+"_comment_reply" ,{display:'block'});
		divCommentContainer.addNode(divCommentReply);
		
		if (this.canPublishAComment == true) {
			var btnRplyComment = new DomNode('button',rootID+"_comment_reply_button" ,{display:'block'});
	
			btnRplyComment.domElement.onclick = function(){
				var e = thisClass.createAReplyEntry(comment["id"]);
				this.style.display="none";
				this.parentElement.parentElement.appendChild(e.domElement);
			};
	
			btnRplyComment.domElement.innerHTML = paella.dictionary.translate("Reply");
			divCommentReply.addNode(btnRplyComment);
		}
		
		for (var i =0; i < comment["replies"].length; ++i ){
			var e = thisClass.createACommentReplyEntry(comment["id"], comment["replies"][i]);
			divCommentContainer.addNode(e);
		}
				
		return divEntry;
	},

	createACommentReplyEntry:function(parentID, comment) {
		var thisClass = this;
		var rootID = this.divPublishComment.identifier+"_entry_" + parentID + "_reply_" + comment["id"];

		var divEntry = new DomNode('div',rootID ,{display:'block'});
		divEntry.domElement.className="comments_entry";

		var divSilhouette = new DomNode('img',rootID+"_silhouette" ,{display:'inline-block'});
		divSilhouette.domElement.src="plugins/silhouette32.png";
		divSilhouette.domElement.className = "comments_entry_silhouette";
		divEntry.addNode(divSilhouette);
		
				
		var divCommentContainer = new DomNode('div',rootID+"_comment_container" ,{display:'inline-block'});
		divCommentContainer.domElement.className = "comments_entry_container";
		divEntry.addNode(divCommentContainer);
				

		var divCommentMetadata = new DomNode('div',rootID+"_comment_metadata" ,{display:'block'});
		divCommentContainer.addNode(divCommentMetadata);
		var datePublish = "";
		if (comment["created"]) {
			var dateToday=new Date()
			var dateComment = paella.utils.timeParse.matterhornTextDateToDate(comment["created"]);			
			datePublish = paella.utils.timeParse.secondsToText((dateToday.getTime()-dateComment.getTime())/1000);
		}		
		
		
		var headLine = "<span class='comments_entry_username'>" + comment["userId"] + "</span>";
		headLine += "<span class='comments_entry_datepublish'>" + datePublish + "</span>";
 
		divCommentMetadata.domElement.innerHTML = headLine;
		
		var divCommentValue = new DomNode('div',rootID+"_comment_value" ,{display:'block'});
		divCommentValue.domElement.className = "comments_entry_comment";
		divCommentContainer.addNode(divCommentValue);		
		
		divCommentValue.domElement.innerHTML = comment["text"];
		
				
		return divEntry;
	},
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Reply Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////	
	createAReplyEntry:function(annotationID) {
		var thisClass = this;
		paella.debug.log("----> " + annotationID)
		var rootID = this.divPublishComment.identifier+"_entry_" + annotationID + "_reply";
		var divEntry = new DomNode('div',rootID+"_entry" ,{display:'block'});
		divEntry.domElement.className="comments_entry";
		
		
		var divSilhouette = new DomNode('img',rootID+"_silhouette" ,{display:'inline-block'});
		divSilhouette.domElement.src="plugins/silhouette32.png";
		divSilhouette.domElement.className = "comments_entry_silhouette";
		divEntry.addNode(divSilhouette);
		
		
		var divCommentContainer = new DomNode('div',rootID+"_reply_container" ,{display:'inline-block'});
		divCommentContainer.domElement.className = "comments_entry_container comments_reply_container";
		divEntry.addNode(divCommentContainer);
		
		var textArea = new DomNode('textarea',rootID+"_textarea" ,{display:'block'});
		divCommentContainer.addNode(textArea);

		this.publishCommentButtons = new DomNode('div',rootID+"_buttons_area" ,{display:'block'});
		//divCommentContainer.domElement.className = "comments_entry_container";
		divCommentContainer.addNode(this.publishCommentButtons);


		var btnAddComment = new DomNode('button',rootID+"_btnAddComment" ,{display:'float', float:'right'});
		btnAddComment.domElement.onclick = function(){thisClass.addReply(annotationID, textArea.domElement.id);};
		btnAddComment.domElement.innerHTML = paella.dictionary.translate("Reply");
		this.publishCommentButtons.addNode(btnAddComment);		
		
		
		return divEntry;
	}
});

/*
episode:6d03cdbb-14ff-4738-b5da-7039bd0c2fb0
type:comment
in:0
value:admin<>un reply<>reply<>2352
out:0
*/
new paella.plugins.CommentsPlugin();


paella.plugins.DebugLog = Class.create(paella.EventDrivenPlugin,{
	enabled:false,
	startTimestamp:0,
	endTimestamp:0,

	getEvents:function() {
		return [paella.events.loadStarted,paella.events.loadComplete];
	},
	
	onEvent:function(eventType,params) {
		switch (eventType) {
			case paella.events.loadStarted:
				this.loadStarted();
				break;
			case paella.events.loadComplete:
				this.loadComplete();
				break;
			case paella.events.play:
				this.play();
				break;
			case paella.events.pause:
				this.pause();
				break;
		}
	},
	
	loadStarted:function() {
		this.startTimestamp = new Date().getTime();
		var userAgent = new UserAgent();
		paella.debug.log("load started");

		paella.debug.log("Operating system: " + userAgent.system.OSName + " " + userAgent.system.Version.stringValue);		
		paella.debug.log("Browser: " + userAgent.browser.Name + " " + userAgent.browser.Version.versionString);
	},

	loadComplete:function() {
		this.endTimestamp = new Date().getTime();
		var userAgent = new UserAgent();
		var loadTime = (this.endTimestamp - this.startTimestamp)/1000;
		paella.debug.log("load complete. Total loading time: " + loadTime + " seconds");
		var os = userAgent.system.OSName;
		var osVersion = userAgent.system.Version.stringValue;
		var browser = userAgent.browser.Name;
		var browserVersion = userAgent.browser.Version.versionString;
// 		new Ajax.Request("save_user_data.php",{
// 			method:'GET',
// 			parameters:{os:os,osVersion:osVersion,browser:browser,browserVersion:browserVersion,loadTime:loadTime},
// 			onSuccess:function(transport) {
// 				
// 			}
// 		});
	},
	
	checkEnabled:function(onSuccess) {
		onSuccess(true);
	},
	
	getIndex:function() {
		return 1000;
	},
	
	getName:function() {
		return "DebugLogPlugin";
	}
});

new paella.plugins.DebugLog();

var frameThumbnail = 'frameThumbnail';
var enabledFrameThumbnail = 'enabledFrameThumbnail';
var focusedFrameThumbnail = 'focusedFrameThumbnail';
var imagePreviewId = 'preview';

var FrameThumbnail = Class.create(DomNode,{
	isCurrentFrame:null,
	frameData:null,
	nextFrameData:null,
	frameContainer:null,

	initialize:function(frameData,frameContainer) {
		paella.debug.log('create frame thumbnail');
		this.parent('img',frameData.id,{float:'left'});
		this.domElement.className = frameThumbnail;
		this.domElement.setAttribute('src',frameData.url);
		this.domElement.setAttribute('height','40');
		this.domElement.setAttribute('width','60');
        this.domElement.setAttribute('role', 'button');
        this.domElement.setAttribute('aria-live', 'polite');
        this.domElement.setAttribute('aria-label', frameData.time + ' seconds');
		this.frameContainer = frameContainer;
		this.frameData = frameData;
		var thisClass = this;
		$(document).bind(paella.events.setTrim,function(event,params) {
			thisClass.checkVisibility(params.trimEnabled,params.trimStart,params.trimEnd);
		});
		$(this.domElement).bind('focusin mouseenter', function(event) {
            $(event.currentTarget).addClass(focusedFrameThumbnail);
            var previewImage = thisClass.frameContainer.frameHiRes[thisClass.frameData.time];
            if (previewImage === undefined){
                previewImage = thisClass.frameContainer.frames[thisClass.frameData.time];
            }
            if (previewImage != undefined){
                var x = event.currentTarget.x;
                var positionX = ($(window).width() / 2) > x ? x : x - 330;
                var y = event.currentTarget.y;
                y = (y <= 200) ? $(event.currentTarget).offset().top : y;
                floatImagePreview(imagePreviewId, previewImage.url, positionX, y - 200);
            }
        });
		$(this.domElement).bind('focusout mouseleave', function(event) {
            $('.' + focusedFrameThumbnail).removeClass(focusedFrameThumbnail);
            $('#' + imagePreviewId).remove();
        });
        $(this.domElement).bind('click keypress', function(event) {
            frameControlPlugin.showFramesPress(event);
        });
	},

	setNextFrameData:function(nextFrameData) {
		this.nextFrameData = nextFrameData;
	},

	setCurrent:function(current) {
		this.isCurrentFrame = current;
		if (current) {
            $('.' + focusedFrameThumbnail).removeClass(focusedFrameThumbnail);
			this.domElement.className = enabledFrameThumbnail;
			$(this.domElement).addClass(focusedFrameThumbnail)
		}
		else {
			this.domElement.className = frameThumbnail;
		}
	},

	checkVisibility:function(trimEnabled,trimStart,trimEnd) {
		if (!trimEnabled) {
			$(this.domElement).show();
		}
		else if (this.frameData.time<trimStart) {
			if (this.nextFrameData && this.nextFrameData.time>trimStart) {
				$(this.domElement).show();
			}
			else {
				$(this.domElement).hide();
			}
		}
		else if (this.frameData.time>trimEnd) {
			$(this.domElement).hide();
		}
		else {
			$(this.domElement).show();
		}
	}
});

var FramesControl = Class.create(DomNode,{
    frames:{},
    frameHiRes:{},
    currentFrame:null,
	hiResFrame:null,

	initialize:function(id) {
		this.parent('div',id,{position:'absolute',left:'0px',right:'0px',bottom:'37px',display:'block'});
		this.domElement.className = 'frameListContainer';
        this.domElement.setAttribute('aria-live', 'polite');
		this.hide();
		var thisClass = this;
		$(document).bind(paella.events.loadComplete,function(event,params) {
			thisClass.setFrames(params.frames);
			thisClass.obtainHiResFrames();
		});
		$(document).bind(paella.events.timeupdate,function(event,params) { thisClass.onTimeUpdate(params) });
		$(document).bind(paella.events.controlBarWillHide,function(event) { thisClass.hide(); });
	},

	seekToTime:function(time) {
		$(document).trigger(paella.events.seekToTime,{time:time});
	},

	isVisible:function() {
		return $(this.domElement).is(':visible');
	},

	show:function() {
		$(this.domElement).show();
	},

	hide:function() {
		$(this.domElement).hide();
	},

	setFrames:function(frames) {
		this.frames = frames;
		var previousFrame = null;
		for(var frame in frames) {
			var frameThumbnail = new FrameThumbnail(frames[frame],this);
			setAriaLabel(frameThumbnail.domElement, describeSeconds(frame));
			frameThumbnail.domElement.setAttribute('time',frame);
			this.addNode(frameThumbnail);
			frameThumbnail.setNextFrameData(previousFrame);
			previousFrame = frame;
		}
	},

	highlightCurrentFrame:function(currentTime) {
		var frame = this.getCurrentFrame(currentTime);
		if (this.currentFrame!=frame) {
			if (this.currentFrame!=null) {
				var lastFrame = this.getNode(this.currentFrame.id);
				lastFrame.setCurrent(false);
			}
			this.currentFrame = frame;
			var frameThumbnail = this.getNode(this.currentFrame.id);
			frameThumbnail.setCurrent(true);
		}
	},

	getCurrentFrame:function(currentTime) {
		return this.frames[this.getCurrentFrameIndex(currentTime)];
	},

	getCurrentFrameIndex:function(currentTime) {
        var currentFrameIndex = 0;
		for (var i=0;i<currentTime;++i) {
			if (this.frames[i]) {
				currentFrameIndex = this.frames[i].time;
			}
		}
		return currentFrameIndex;
	},

	onTimeUpdate:function(memo) {
		if (this.isVisible()) {
			this.highlightCurrentFrame(memo.currentTime);
		}
	},

	obtainHiResFrames:function() {
		var unorderedFrames = {}
		var attachments = paella.matterhorn.episode.mediapackage.attachments.attachment;
		var lastFrameTime = 0;
		for (var attachment in attachments) {
			attachment = attachments[attachment];
			if (attachment.type=='presentation/segment+preview') {
				var url = attachment.url;
				var time = 0;
				if (/time=T(\d+):(\d+):(\d+)/.test(attachment.ref)) {
					time = parseInt(RegExp.$1)*60*60 + parseInt(RegExp.$2)*60 + parseInt(RegExp.$3);
				}
				unorderedFrames[time] = {time:time,url:url,mimetype:attachment.mimetype,id:attachment.id};
				if (time>lastFrameTime) lastFrameTime = time;
			}
		}

		for (var i=0;i<=lastFrameTime;++i) {
			if (unorderedFrames[i]) {
				this.frameHiRes[i] = unorderedFrames[i];
			}
		}
	}
});

paella.plugins.FrameControlPlugin = Class.create(paella.PlaybackPopUpPlugin,{
	framesControl:null,
	container:null,
	button:null,

	getTabName:function() {
		return 'Diapositivas';
	},

	getRootNode:function(id) {
        $(document).keyup(function(event) {
            if (thisClass.framesControl.isVisible()) {
                thisClass.keyUp(event);
            }
        });
		var thisClass = this;
		this.container = new DomNode('div',id + '_frameControl_container');
		this.button = this.container.addNode(new Button(id + '_frameControl_button','showFramesButton',function(event) { thisClass.showFramesPress(event); },true));
        this.button.domElement.setAttribute('tabindex', 60);
        setAriaLabel(this.button.domElement, 'Navigate by slides');
		return this.container;
	},

    isRelevantToCurrentVideo:function() {
        return this.isEpisodeWithSegments();
    },

    isEpisodeWithSegments:function() {
        var segmentCount = 0;
        var attachments = paella.matterhorn.episode.mediapackage.attachments.attachment;
        for (var i=0; i < attachments.length; i++) {
            if (attachments[i].type.indexOf('segment') > -1) {
                segmentCount++;
            }
        }
        return (segmentCount > 0);
    },

	getWidth:function() {
		return 45;
	},

	setRightPosition:function(position) {
		this.button.domElement.style.right = position + 'px';
	},

	getPopUpContent:function(id) {
		var thisClass = this;
		this.framesControl = new FramesControl(id + '_frameContol_frames');
		$(document).bind(paella.events.seekToTime,function(event){
			if (thisClass.showFramesButton().isToggled()) {
				thisClass.showFramesButton().toggleIcon();
			}
		});
		return this.framesControl;
	},

    escapePopUp:function() {
        $('#' + imagePreviewId).remove();
		this.framesControl.hide();
        this.button.toggle();
    },

	showFramesButton:function() {
		return this.button;
	},

	showFramesPress:function(event) {
		if (event.type=="click" || event.which==13) {
			if (this.framesControl.isVisible()) {
                $('#' + imagePreviewId).remove();
                var frame = event.type=="click"? $(event.target): $('.focusedFrameThumbnail');
                if (frame && frame.attr('time')) {
                    var duration = paella.player.videoContainer.duration();
                    // We add one (1) to the time to make sure we don't land on previous slide.
                    var time = parseInt(frame.attr('time')) + 1;
                    changeVideoPositionPercentage(time * 100 / duration);
                    frame.attr('aria-label', 'Skip to '+~~(time/60)+' minutes and '+(time % 60)+' seconds');
                    this.framesControl.highlightCurrentFrame(time);
                    this.framesControl.seekToTime(time);
                }
				this.framesControl.hide();
	            setAriaLabel(this.button.domElement, 'Hide slides');
			}
			else {
                if (viewModePlugin && viewModePlugin.isContainerVisible()) {
                    viewModePlugin.escapePopUp();
                }
				this.framesControl.show();
				var hasActiveFrame = this.framesControl && this.framesControl.currentFrame != null;
				var time = hasActiveFrame ? this.framesControl.currentFrame.time : 0;
				this.framesControl.highlightCurrentFrame(time);
	            setAriaLabel(this.button.domElement, 'Show slides');
			}
		}
	},

    getCurrentFocus:function() {
        var current = $('.' + focusedFrameThumbnail);
        if (current.length == 0) {
            current = $('.' + frameThumbnail)[0];
        }
        return $(current);
    },

	checkEnabled:function(onSuccess) {
	    onSuccess(true);
	},

	getIndex:function() {
		return 100;
	},

	getName:function() {
		return 'FrameControlPlugin';
	},

	getMinWindowSize:function() {
		return 600;
	}

});

var frameControlPlugin = new paella.plugins.FrameControlPlugin();


function isRecordingWithVideo() {
    var hasVideo = false;
    var tracks = paella.matterhorn.episode.mediapackage.media.track;
    for (var i=0; i < tracks.length; i++) {
        var mimetype = tracks[i].mimetype;
        if (mimetype && mimetype.indexOf('video') !== -1) {
            hasVideo = true;
            break;
        }
    }
    return hasVideo;
};

paella.plugins.FullscreenPlugin = Class.create(paella.PlaybackPopUpPlugin,{
	button:null,

	getRootNode:function(id) {
		var thisClass = this;
		this.button = new Button(id + '_fullscreen_button','fullscreenButton',function(event) { thisClass.switchFullscreen(); }, false);
        setAriaLabel(this.button.domElement, "Full screen");
        this.button.domElement.setAttribute("tabindex", "50");
		return this.button;
	},

	getWidth:function() {
		return 45;
	},

	setRightPosition:function(position) {
		this.button.domElement.style.right = position + 'px';
	},

	getPopUpContent:function(id) {
		return null;
	},

	isFullscreen:function() {
		if (document.webkitIsFullScreen!=undefined) {
			return document.webkitIsFullScreen;
		}
		else if (document.mozFullScreen!=undefined) {
			return document.mozFullScreen;
		}
		return false;
	},

	switchFullscreen:function() {
		var fs = document.getElementById(paella.player.mainContainer.id);
		fs.style.width = '100%';
		fs.style.height = '100%';
		if (this.isFullscreen()) {
			if (document.webkitCancelFullScreen) {
				document.webkitCancelFullScreen();
			}
			else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			}
			else if (document.cancelFullScreen) {
				document.cancelFullScreen();
			}
		}
		else {
			if (fs.webkitRequestFullScreen) {
				fs.webkitRequestFullScreen();
				this.fullscreen = true;
			}
			else if (fs.mozRequestFullScreen){
				fs.mozRequestFullScreen();
				this.fullscreen = true;
			}
			else if (fs.requestFullScreen()) {
				fs.requestFullScreen();
				this.fullscreen = true;
			}
			else {
				alert('Your browser does not support fullscreen mode');
			}
		}
	},

	isRelevantToCurrentVideo:function() {
		return isRecordingWithVideo();
	},

	checkEnabled:function(onSuccess) {
		onSuccess(isRecordingWithVideo());
	},

	getIndex:function() {
		return 103;
	},

	getName:function() {
		return "FullScreenButtonPlugin";
	}
});

new paella.plugins.FullscreenPlugin();



paella.plugins.PlayPauseButtonPlugin = Class.create(paella.PlaybackControlPlugin,{
	playId:'',
	pauseId:'',
	containerId:'',
	container:null,

	getRootNode:function(id) {
		this.playId = id + '_playButton';
		this.pauseId = id + '_pauseButton';
		this.containerId = id + '_container';
		var playPauseContainer = new DomNode('div',this.containerId,{position:'absolute'});
		this.container = playPauseContainer;

		var anchorTag = new DomNode('a','hidden_videoPlayerControls',{});
		anchorTag.domElement.setAttribute('name','videoPlayerControls');
		anchorTag.domElement.setAttribute('class','hide');
		playPauseContainer.addNode(anchorTag);

		var thisClass = this;
		var playButton = new Button(this.playId,'playButton',function(event) { thisClass.playButtonClick(); }, false);
		playButton.domElement.setAttribute("tabindex", "5");

		setAriaLabel(playButton.domElement, "Play " + getRecordingTitle());
		playPauseContainer.addNode(playButton);
		var pauseButton = new Button(this.pauseId,'pauseButton',function(event) { thisClass.pauseButtonClick(); },false);
		pauseButton.domElement.setAttribute("tabindex", "5");
		setAriaLabel(pauseButton.domElement, "Pause " + getRecordingTitle());
		playPauseContainer.addNode(pauseButton);
		$(pauseButton.domElement).hide();

		$(document).bind(paella.events.endVideo,function(event) {
			thisClass.playButton().show();
			thisClass.pauseButton().hide();
		});

		$(document).bind(paella.events.play,function() {
			thisClass.onPlay();
		});
		$(document).bind(paella.events.pause,function() {
			thisClass.onPause();
		});

		return playPauseContainer;
	},

	setLeftPosition:function(position) {
		this.container.domElement.style.left = position + 'px';
	},

	getWidth:function() {
		return 50;
	},

	checkEnabled:function(onSuccess) {
		onSuccess(true);
	},

	getIndex:function() {
		return 0;
	},

	getName:function() {
		return "PlayPauseButtonPlugin";
	},

	playButton:function() {
		return this.container.getNode(this.playId);
	},

	pauseButton:function() {
		return this.container.getNode(this.pauseId);
	},

	playButtonClick:function() {
		this.playButton().hide();
		this.pauseButton().show();
		$(document).trigger(paella.events.play);
		setAriaLabel(this.pauseButton().domElement, "Video is now playing");
	},

	pauseButtonClick:function() {
		this.playButton().show();
		this.pauseButton().hide();
		$(document).trigger(paella.events.pause);
		setAriaLabel(this.playButton().domElement, "Video has been paused");
	},

	onPlay:function() {
		if (this.playButton()) {
			this.playButton().hide();
			this.pauseButton().show();
		}
	},

	onPause:function() {
		if (this.playButton()) {
			this.playButton().show();
			this.pauseButton().hide();
		}
	}
});

paella.plugins.playPauseButtonPlugin = new paella.plugins.PlayPauseButtonPlugin();

paella.plugins.PlayButtonOnScreen = Class.create(paella.EventDrivenPlugin,{
	containerId:'paella_plugin_PlayButtonOnScreen',
	container:null,
	enabled:true,
	isPlaying:false,

	initPlugin:function() {
		this.container = document.createElement('div');
		this.container.className = "playButtonOnScreen";
		this.container.id = this.containerId;
		paella.player.videoContainer.domElement.appendChild(this.container);
		var thisClass = this;
		$(this.container).click(function(event){thisClass.onPlayButtonClick()});

		var icon = document.createElement('canvas');
		icon.className = "playButtonOnScreenIcon";
		icon.setAttribute("width", 300);
		icon.setAttribute("height",300);
		var ctx = icon.getContext('2d');

		ctx.beginPath();
		ctx.arc(150,150,140,0,2*Math.PI,true);
		ctx.closePath();

		ctx.strokeStyle = 'white';
		ctx.lineWidth = 10;
		ctx.stroke();
		ctx.fillStyle = '#8f8f8f';
		ctx.fill();

		ctx.beginPath();
		ctx.moveTo(100,70);
		ctx.lineTo(250,150);
		ctx.lineTo(100,230);
		ctx.lineTo(100,70);
		ctx.closePath();
		ctx.fillStyle = 'white';
		ctx.fill();

		ctx.stroke();

		this.container.appendChild(icon);
	},

	getEvents:function() {
		return [paella.events.endVideo,paella.events.play,paella.events.pause,paella.events.showEditor,paella.events.hideEditor,paella.events.loadComplete];
	},

	onEvent:function(eventType,params) {
		switch (eventType) {
			case paella.events.loadComplete:
				this.initPlugin();
				break;
			case paella.events.endVideo:
				this.endVideo();
				break;
			case paella.events.play:
				this.play();
				break;
			case paella.events.pause:
				this.pause();
				break;
			case paella.events.showEditor:
				this.showEditor();
				break;
			case paella.events.hideEditor:
				this.hideEditor();
				break;
		}
	},

	onPlayButtonClick:function() {
		$(document).trigger(paella.events.play);
	},

	endVideo:function() {
		this.isPlaying = false;
		this.checkStatus();
	},

	play:function() {
		this.isPlaying = true;
		this.checkStatus();
	},

	pause:function() {
		this.isPlaying = false;
		this.checkStatus();
	},

	showEditor:function() {
		this.enabled = false;
		this.checkStatus();
	},

	hideEditor:function() {
		this.enabled = true;
		this.checkStatus();
	},

	checkStatus:function() {
		if ((this.enabled && this.isPlaying) || !this.enabled) {
			$(this.container).hide();
		}
		else {
			$(this.container).show();
		}
	},

	checkEnabled:function(onSuccess) {
		onSuccess(true);
	},

	getIndex:function() {
		return 1010;
	},

	getName:function() {
		return "PlayButtonOnScreen";
	}
});

new paella.plugins.PlayButtonOnScreen();


paella.plugins.recess = {classes:{}, instances:{}, recessTracks:[] };


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Recess Loader
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
paella.plugins.recess.classes.RecessLoader = Class.create({
	
	initialize:function() {
		var thisClass = this;
		$(document).bind(paella.events.loadComplete,function(event,params) {			
			thisClass.loadRecessInfo();
		});		
	},
	
	loadRecessInfo:function() {
		var thisClass = this;
		this.loadAttachmentData(paella.matterhorn.episode.id, "paella/recess", function(data){ 
			paella.plugins.recess.recessTracks = thisClass.xmlToTracks(data);
		});				
	},
	
	saveRecessInfo:function() {
		var commentValue = this.tracksToXML(paella.plugins.recess.recessTracks);
		paella.plugins.recess.instances.recessLoader.saveAttachmentData(paella.matterhorn.episode.id, "paella/recess", commentValue);
	},
	
	tracksToXML:function(tracks) {
		var xml = '<?xml version="1.0" encoding="UTF-8"?>';
		xml = xml + '<tracks>'
	
		for (var i = 0; i<tracks.length; i=i+1) {
			var t = tracks[i];
			xml = xml + '<track id="'+ t.id +'" start="'+ t.s +'" end="'+ t.e +'" />'
		}
		xml = xml + '</tracks>'
		return xml;
	},
	
	xmlToTracks:function(xmlText) {
		var tracks = [];
		var xml = null;
		try {
			xml = $(xmlText);
			var xml_tracks = xml.find("track");
			for (var i=0; i< xml_tracks.length; i++) {		
				var t = xml_tracks[i];
				var t_id = t.getAttribute("id")
				var t_start = t.getAttribute("start")
				var t_end = t.getAttribute("end")
				tracks.push({id:t_id,s:t_start,e:t_end});
			}	
		}
		catch(e){ 
			tracks = [];
		}
		return tracks;
	},
	
	loadAttachmentData:function(episodeid, type, onSuccess, onError){
		var loader = new paella.matterhorn.LoaderSaverInfo(paella.player.config);
		loader.loadData(episodeid, type, onSuccess, onError);
	},
	
	saveAttachmentData:function(episodeid, type, value, onSuccess, onError){
		var saver = new paella.matterhorn.LoaderSaverInfo(paella.player.config);
		saver.saveData(episodeid, type, value, onSuccess, onError);
	}

});

paella.plugins.recess.instances.recessLoader = new paella.plugins.recess.classes.RecessLoader();


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Editor Recess Plugin
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
paella.plugins.recess.classes.RecessTrackPlugin = Class.create(paella.editor.MainTrackPlugin,{
	selectedItem:0,
	

	initialize:function() {
		this.parent();
		var thisClass = this;
		if (paella.utils.language()=='es') {
			var esDict = {
				'Recess':'Descanso',
				'Create':'Añadir',
				'Create a new break in the current position': 'Añade un descanso en el instante actual',
				'Delete':'Borrar',
				'Delete selected break': 'Borra el descanso seleccionado'
			};
			paella.dictionary.addDictionary(esDict);
		}
		
		$(document).bind(paella.events.showEditor ,function(event,params) {			
			thisClass.makeTracksBackup();
		});	
		
		$(document).bind(paella.events.hideEditor ,function(event,params) {
			paella.plugins.recess.recessTracks = paella.plugins.recess.recessTracksBackup;
		});	
		
	},

	makeTracksBackup:function() {
		paella.plugins.recess.recessTracksBackup = jQuery.extend(true, {}, paella.plugins.recess.recessTracks);
	},
		
	getTrackItems:function() {
		return paella.plugins.recess.recessTracks;
	},
	
	getName:function() {
		return "RecessTrackPlugin";
	},
	
	getTrackName:function() {
		return paella.dictionary.translate("Recess");
	},
	
	getColor:function() {
		return 'rgb(212, 51, 97)';
	},
	
	onSelect:function(trackData) {
		this.selectedItem = trackData.id;
	},
	
	onUnselect:function() {
		this.selectedItem = 0;
	},
	
	onDblClick:function(trackData) {
	},
	
	getTools:function() {
		return [
			{name:'create',label:paella.dictionary.translate('Create'),hint:paella.dictionary.translate('Create a new break in the current position')},
			{name:'delete',label:paella.dictionary.translate('Delete'),hint:paella.dictionary.translate('Delete selected break')},
			{name:'lock',label:paella.dictionary.translate('Lock'),hint:paella.dictionary.translate('Lock selected break')},
			{name:'unlock',label:paella.dictionary.translate('Unlock'),hint:paella.dictionary.translate('Unlock selected break')}
		];
	},
	
	onTrackChanged:function(id,start,end) {
		var item = this.getItem(id);
		if (item) {
			item.s = start;
			item.e = end;
		}
	},

	onToolSelected:function(toolName) {
		var selectedTrackIndex = this.getSelectedItemIndex();
		if (toolName=='delete' && selectedTrackIndex!=-1) {
			if (!paella.plugins.recess.recessTracks[selectedTrackIndex].lock) {
				paella.plugins.recess.recessTracks.splice(selectedTrackIndex,1);
			}
			return true;
		}
		else if (toolName=='create') {
			var start = paella.player.videoContainer.currentTime();
			var end = start + 60;
			var id = this.getTrackUniqueId();
			paella.plugins.recess.recessTracks.push({id:id,s:start,e:end,content:paella.dictionary.translate('Recess')});
			return true;
		}
		else if (toolName=='lock') {
			paella.plugins.recess.recessTracks[selectedTrackIndex].lock = true;
			return true;
		}
		else if (toolName=='unlock') {
			paella.plugins.recess.recessTracks[selectedTrackIndex].lock = false;
			return true;
		}
	},
	
	getItem:function(id) {
		for (var i=0;i<paella.plugins.recess.recessTracks.length;++i) {
			if (paella.plugins.recess.recessTracks[i].id==id) {
				return paella.plugins.recess.recessTracks[i];
			}
		}
		return null;
	},
	
	getSelectedItemIndex:function() {
		for (var i=0;i<paella.plugins.recess.recessTracks.length;++i) {
			if (paella.plugins.recess.recessTracks[i].id==this.selectedItem) {
				return i;
			}
		}
		return -1;
	},
	
	getTrackUniqueId:function() {
		var newId = -1;
		for (var i=0;i<paella.plugins.recess.recessTracks.length;++i) {
			if (newId<=paella.plugins.recess.recessTracks[i].id) {
				newId = paella.plugins.recess.recessTracks[i].id + 1;
			}
		}
		return newId;
	},
			
	onSave:function(onSuccess) {
		this.makeTracksBackup();
		paella.plugins.recess.instances.recessLoader.saveRecessInfo();
		onSuccess(true);
	},
		
	contextHelpString:function() {
		// TODO: Implement this using the standard paella.dictionary class
		if (paella.utils.language()=="es") {
			return "Utiliza la herramienta de descansos para marcar los descansos de la clase. Para cambiar la duración solo hay que arrastrar el inicio o el final de la pista \"Descanso\", en la linea de tiempo.";
		}
		else {
			return "Use this tool to define the breaks.";
		}
	}
});

paella.plugins.recess.instances.recessTrackPlugin = new paella.plugins.recess.classes.RecessTrackPlugin();



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Player Recess Plugin
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
paella.plugins.recess.classes.RecessPlayerPlugin = Class.create(paella.EventDrivenPlugin,{
	recessFrame: null,
	jumpFromSeek: false,

	initialize:function() {
		this.parent();
		this.recessFrame = document.createElement("div");
        this.recessFrame.setAttribute('style', 'width: 100%; height:100%; background-color:rgba(0,0,0,0.8); z-index:10000; display:none;');
	},
	
	checkEnabled:function(onSuccess) {
		onSuccess(true);
	},
	
	getIndex:function() {
		return 1000;
	},
	
	getName:function() {
		return "RecessPlayerPlugin";
	},

	getEvents:function() {
		return [paella.events.loadComplete, paella.events.play, paella.events.pause, paella.events.seekToTime, paella.events.seekTo];
	},
	
	onEvent:function(eventType,params) {
		switch (eventType) {
			case paella.events.loadComplete:
				this.loadComplete();
				break;
			case paella.events.play:
				this.startTimer();
				break;
			case paella.events.pause:
				this.pauseTimer();
				break;
			case paella.events.seekToTime:
				this.seekToTime(params.time);
				break;
			case paella.events.seekTo:
				var time = (paella.player.videoContainer.duration(!(paella.player.videoContainer.trimEnabled())) * params.newPositionPercent) / 100;
				if (paella.player.videoContainer.trimEnabled()){
					time = time + paella.player.videoContainer.trimming.start;
				}
				this.seekToTime(time);
				break;
		}
	},
	
	loadComplete:function() {
        this.recessFrame.innerHTML = '<span style="bottom:50%; position:absolute; width:100%; color:rgb(241, 203, 0); font-size:70px; text-align:center;">'+paella.dictionary.translate('Recess')+'<span>';				
		var overlayContainer = paella.player.videoContainer.overlayContainer;
		overlayContainer.addElement(this.recessFrame, overlayContainer.getMasterRect());
	},
	
	startTimer:function() {
		var thisClass = this;
		this.timer = new paella.utils.Timer(function(timer) {
			var time = paella.player.videoContainer.currentTime();
			thisClass.onUpdateRecess(time);
			},1000.0);
		this.timer.repeat = true;	
	},
	
	pauseTimer:function() {
		if (this.timer!=null) {
			this.timer.cancel();
			this.timer = null;
		}
	},
	
	seekToTime:function(time) {
		this.jumpFromSeek = true;
		this.onUpdateRecess(time);
	},
	
	onUpdateRecess:function(time) {
		var isRecess = false;
		
		for (var i=0; i<paella.plugins.recess.recessTracks.length; i++){
			var recess = paella.plugins.recess.recessTracks[i];
			if ((recess.s < time) && (time < recess.e)) {
				isRecess = true;
				break;						
			}
		}
		if (isRecess == true){
			if (this.jumpFromSeek == false){
				$(document).trigger(paella.events.seekToTime,{time:recess.e});
			}
			else {
				if (this.recessFrame.style.display != "block") {
					this.recessFrame.style.display="block";
				}
			}
		}
		else {
			this.jumpFromSeek = false;
			if ((this.recessFrame) && (this.recessFrame.style.display != "none")){
				this.recessFrame.style.display="none";
			}
		}
	}	
});

paella.plugins.recess.instances.recessPlayerPlugin = new paella.plugins.recess.classes.RecessPlayerPlugin();








paella.plugins.RepeatButtonPlugin = Class.create(paella.PlaybackControlPlugin,{
	buttonId:'',
	button:null,

	getRootNode:function(id) {
		this.buttonId = id + '_repeat_button';
		var thisClass = this;
		this.button = new Button(this.buttonId,'repeatButton',function(event) { thisClass.repeatButtonClick() });
		setAriaLabel(this.button.domElement, "Rewind 30 seconds");
        this.button.domElement.setAttribute("tabindex", "20");
		return this.button;
	},

	getWidth:function() {
		return 50;
	},

	setLeftPosition:function(left) {
		this.button.domElement.style.left = left + 'px';
		this.button.domElement.style.position = 'absolute';
	},

	repeatButtonClick:function() {
	    changeVideoPosition(-30);
	},

	checkEnabled:function(onSuccess) {
		onSuccess(true);
	},

	getIndex:function() {
		return 2;
	},

	getName:function() {
		return "RepeatButtonPlugin";
	},

	getMinWindowSize:function() {
		return 750;
	}
});

new paella.plugins.RepeatButtonPlugin();


var viewModeFocusClass = 'viewModeThumbnailFocus';

var ProfileItemButton = Class.create(DomNode,{
    viewModePlugin:null,

    initialize:function(icon,profileName,viewModePlugin) {
        this.parent('div',profileName + '_button',{display:'block',backgroundImage:'url(' + icon + ')',width:'78px',height:'41px'});
        this.viewModePlugin = viewModePlugin;
        var thisClass = this;
        this.domElement.setAttribute('aria-live','polite');
        this.domElement.setAttribute('role','button');
        this.domElement.setAttribute('aria-label', profileName.replace('_',' '));
        $(this.domElement).on('focusin mouseenter', function(event) {
            $(event.currentTarget).addClass(viewModeFocusClass);
        });
        $(this.domElement).on('focusout mouseleave', function(event) {
            $(event.currentTarget).removeClass(viewModeFocusClass);
        });
        $(this.domElement).bind('click keyup', function(event) {
            viewModePlugin.viewModePress(event);
        });
    }
});

paella.plugins.ViewModePlugin = Class.create(paella.PlaybackPopUpPlugin,{
    viewModeContainer:'',
    button:'',

    getRootNode:function(id) {
        var thisClass = this;
        this.button = new Button(id + '_view_mode_button','showViewModeButton',function(event) { thisClass.viewModePress(event) },true);
        this.button.domElement.setAttribute('tabindex',55);
        $(document).keyup(function(event) {
            if (thisClass.button.isToggled()) {
                thisClass.keyUp(event);
            }
        });
        setAriaLabel(this.button.domElement, 'Change video layout');
        return this.button;
    },

    getCurrentFocus:function() {
        var current = $('.' + viewModeFocusClass);
        if (current.length == 0) {
            current = $('#' + paellaPlayer.selectedProfile + '_button');
        }
        return current;
    },

    getWidth:function() {
        return 45;
    },

    isContainerVisible:function() {
        return $(this.viewModeContainer.domElement).is(':visible');
    },

    setRightPosition:function(position) {
        this.button.domElement.style.right = position + 'px';
    },

    getPopUpContent:function(id) {
        var thisClass = this;
        this.viewModeContainer = new DomNode('div',id + '_viewmode_container',{display:'none'});
        paella.Profiles.loadProfileList(function(profiles) {
            for (var profile in profiles) {
                var profileData = profiles[profile];
                var imageUrl = 'config/profiles/resources/' + profileData.icon;
                thisClass.viewModeContainer.addNode(new ProfileItemButton(imageUrl,profile,thisClass));
                // Profile icon preload
                var image = new Image();
                image.src = imageUrl;
            }
        });
        return this.viewModeContainer;
    },

    viewModePress:function(event) {
        if (event.type=='click' || event.which==13) {
            if (!this.isContainerVisible()) {
                if (frameControlPlugin && frameControlPlugin.framesControl.isVisible()) {
                    frameControlPlugin.escapePopUp();
                }
                $(this.viewModeContainer.domElement).show();
                this.highlightNewProfileButton(null, paellaPlayer.selectedProfile);
            }
            else {
                var focus = $('.viewModeThumbnailFocus');
                if (focus) {
                    var focusId = focus.attr('id');
                    if (focusId) {
                        var currentProfileName = paellaPlayer.selectedProfile;
                        profileName=focusId.replace('_button','')
                        if (profileName!=currentProfileName) {
                            this.highlightNewProfileButton(currentProfileName, profileName);
                            paellaPlayer.setProfile(profileName);
                        }
                    }
                }
                $(this.viewModeContainer.domElement).hide();
            }
        }
    },

    escapePopUp:function() {
        $(viewModePlugin.viewModeContainer.domElement).hide();
        viewModePlugin.button.toggle();
    },

    isRelevantToCurrentVideo:function() {
        return this.isRecordingWithTwoVideoStreams();
    },

    isRecordingWithTwoVideoStreams:function() {
        var hasPresenter = false;
        var hasPresentation = false;
        var tracks = paella.matterhorn.episode.mediapackage.media.track;
        for (var i=0; i < tracks.length; i++) {
            var type = tracks[i].type;
            if (type.indexOf('presenter') !== -1) {
                hasPresenter = true;
            } else if (type.indexOf('presentation') !== -1) {
                hasPresentation = true;
            }
            if (hasPresenter && hasPresentation) {
                break;
            }
        }
        return hasPresenter && hasPresentation;
    },

    checkEnabled:function(onSuccess) {
        onSuccess(!paella.player.videoContainer.isMonostream);
    },

    getIndex:function() {
        return 101;
    },

    getName:function() {
        return 'ViewModePlugin';
    },

    getMinWindowSize:function() {
        return 500;
    },

    highlightNewProfileButton:function(currentProfileName, newProfileName) {
        $('.' + viewModeFocusClass).removeClass(viewModeFocusClass);
        if (currentProfileName != null) {
            var currentButtonId = currentProfileName + '_button';
            var currentButton = $('#' + currentButtonId);
            $(currentButton).css({'background-position':'0px 0px'});
            $(currentButton).attr('aria-label', currentProfileName.replace('_',' '));
        }
        var newButtonId = newProfileName + '_button';
        var newButton = $('#' + newButtonId);
        $(newButton).css({'background-position':'-78px 0px'});
        $(newButton).attr('aria-label', newProfileName.replace('_',' ') + ' video layout applied');
    }

});

var viewModePlugin = new paella.plugins.ViewModePlugin();


