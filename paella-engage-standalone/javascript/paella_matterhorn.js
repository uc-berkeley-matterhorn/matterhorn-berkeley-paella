var MyInitDelegate = Class.create(paella.InitDelegate,{
    // Example: customize id
    getId:function() {
        return paella.utils.parameters.get('id');
    },

    // Example: customize config settings
    loadConfig:function(onSuccess) {
        this.parent(function(data) {
            //var server = paella.utils.parameters.get('server');
            //if (server != "") {
            //	data.restServer.url = server;
            //}
            onSuccess(data);
        });
    }
});

var recordingTitle;
var recordingStartDatetime;
var seriesTitle;

function getRecordingStartDatetime() {
    return recordingStartDatetime;
}

function getRecordingTitle() {
    return recordingTitle;
}

function setAriaLabel(domElement, altText) {
    domElement.setAttribute("alt", "");
    domElement.setAttribute("title", altText);
    domElement.setAttribute("aria-label", altText);
}

function getCompliantURL(url) {
    var documentURL = document.URL.toLowerCase();
    var compliantURL = url;
    if (documentURL.indexOf('https') > -1) {
        compliantURL = url.replace("http://","https://");
        compliantURL = url.replace("rtmp://","rtmps://");
    }
    return compliantURL;
}

function describeSeconds(seconds){
    var label = "";
    if (seconds == 0) {
        label = "0 seconds";
    } else {
        var remainder = seconds % 60;
        if (seconds >= 60) {
            label += Math.floor(seconds / 60) + " minutes";
            label = (remainder > 0) ? label + " and " : label;
        }
        if (remainder > 0) {
            label += remainder + " seconds";
        }
    }
    return label;
}

function formatMediapackageDate(dateString){
    var timeDate;
    if (dateString) {
        var sd = new Date();
        sd.setFullYear(parseInt(dateString.substring(0, 4), 10));
        sd.setMonth(parseInt(dateString.substring(5, 7), 10) - 1);
        sd.setDate(parseInt(dateString.substring(8, 10), 10));
        var dateWrapper = moment(sd);
        timeDate = dateWrapper.format("dddd, MMMM D, YYYY");
    } else {
        timeDate = "The date is not available."
    }
    return timeDate;
}

function isFramesControlVisible(){
    return frameControlPlugin && frameControlPlugin.framesControl && frameControlPlugin.framesControl.isVisible();
}

function getFrameCount(){
    return isFramesControlVisible() ? Object.keys(frameControlPlugin.framesControl.frames).length : null;
}

function getIndexOfCurrentFrame(){
    return (isFramesControlVisible() && frameControlPlugin.framesControl.currentFrame != null)
        ? Object.keys(frameControlPlugin.framesControl.frames).indexOf(frameControlPlugin.framesControl.currentFrame.time + "")
        : 0;
}

function getFrameWithIndex(frameIndex){
    var frame = null;
    var frameTime = Object.keys(frameControlPlugin.framesControl.frames)[frameIndex];
    if (frameTime) {
        var frameId = frameControlPlugin.framesControl.frames[frameTime].id;
        var frameDomElement = frameControlPlugin.framesControl.getNode(frameId).domElement;
        frame = frameDomElement;
    }
    return frame;
}

function getPreviousFrame(){
    var currentIndex = getIndexOfCurrentFrame();
    return (currentIndex != null && currentIndex != 0) ? getFrameWithIndex(currentIndex - 1) : null;
}

function getNextFrame(){
    var currentIndex = getIndexOfCurrentFrame();
    return (currentIndex != null && currentIndex < getFrameCount()) ? getFrameWithIndex(currentIndex + 1) : null;
}

function onHeaderSearch(){
    var headerSearchInput = $('#paellaExtendedHeaderSearchInput')[0]
    window.location.href="?q="+headerSearchInput.value;
}

function loadPaella() {
    var initDelegate = null;
    var initDelegate = new MyInitDelegate({accessControl:new paella.matterhorn.AccessControl(),videoLoader:new paella.matterhorn.VideoLoader()});

    var params = {};

    var q = paella.utils.parameters.get('q');
    if (q !== "") {
      params.q = decodeURIComponent(q);
      if (params.q == undefined) params.q ="";
    }
    else{
        params.q="";
    }

    var sid = paella.utils.parameters.get('sid');
    if (sid !== "") {
      params.sid = sid;
    }

    var limit = paella.utils.parameters.get('limit');
    limit = parseInt(limit);
    if (isNaN(limit)) {
      limit = 100000;
    }
    params.limit = limit;

    var page = paella.utils.parameters.get('page');
    page = parseInt(page);
    if (isNaN(page)) {
      page = 1;
    }
    params.offset = (page - 1) * limit;

    // var headerSearchInput = $('#paellaExtendedHeaderSearchInput')[0]
    // headerSearchInput.value = params.q;

    initDelegate.loadConfig(function(config){
        var myClass = new paella.matterhorn.SearchEpisode(config, params);
        myClass.doSearch(params, document.getElementById('listContainer'));
    })
}

/*
 * Image preview script was written by Alen Grakalic (http://cssglobe.com)
 * for more info visit http://cssglobe.com/post/1695/easiest-tooltip-and-image-preview-using-jquery
 */
function imagePreview() {
    imagePreviewByClassName("a", "preview");
}

function imagePreviewByClassName(elementType, className) {
    $(elementType + "." + className).hover(function(e) {
        var pageX = e.pageX ? e.pageX : e.target.x;
        floatImagePreview(className, e.target.src, getPreviewLeft(pageX), getPreviewBottom(e));
    },
    function() {
        $("#" + className).remove();
    });
    $(elementType + "." + className).mousemove(function(e){
        var pageX = e.pageX ? e.pageX : e.target.x;
        $("#" + className)
            .css("left", getPreviewLeft(pageX) + "px")
            .css("top", getPreviewBottom(e) + "px");
    });
};

function getPreviewLeft(pageX){
    var offset = 30;
    var isTargetOnLeft = ($(window).width() / 2) > pageX;
    return isTargetOnLeft ? pageX + offset : pageX - 320 - offset;
};

function getPreviewBottom(e) {
    var pageY = e.pageY ? e.pageY : e.target.y;
    var maxY = $(window).height() - 150;
    return pageY > maxY ? maxY : pageY;
};

function floatImagePreview(name, imgSrc, pageX, pageY) {
    // These variable determine popup's distance from the cursor
    var topPx = pageY - 60;
    var leftPx = pageX + 30;
    $("body").append("<span id='" + name + "' style='z-index: 10000000000'><img src='"+ imgSrc +"' width='320' alt='' /></span>");
    $("#" + name)
        .css("top", topPx + "px")
        .css("left", leftPx + "px")
        .fadeIn("fast");
};

paella.matterhorn = {
	series:{
		serie:null,
		acl:null
	},
	episode:null,
	me:null,
	access:{write:false,read:false,contribute:false,isAnonymous:false}
};

paella.matterhorn.UserRoleManager = Class.create({
	serverUrl:"",
	proxyUrl:false,
	useJsonp:false,

	initialize:function(config) {
		this.serverUrl = config.restServer.url;
		if (config.proxyLoader && config.proxyLoader.enabled) {
			this.proxyUrl = config.proxyLoader.url;
		}
		this.useJsonp = config.proxyLoader.usejsonp;
	},

	checkAccess:function(onSuccess) {
		//onSuccess(true,true);
		var thisClass = this;
		var id = paella.utils.parameters.get('id');

		thisClass.loadUserRoles(function(roleInfo) {
			var userRoles = new Array();
			var adminRole = roleInfo.org.adminRole;
			var anonymousRole = roleInfo.org.anonymousRole;
			var isAnonymous = true;
			if (roleInfo.roles) {
				for(var i = 0; i<roleInfo.roles.length; ++i) {
					var roleName = roleInfo.roles[i];
					userRoles.push(roleName);
					if (roleName!=anonymousRole) {
						isAnonymous = false;
					}
				}
			}

			thisClass.getPermissions(id,function(data) {
				var canRead = false;
				var canContribute = false;
				var canWrite = false;
				var loadError = false;
				if (data && data.acl) {
					paella.matterhorn.series.acl = data.acl;
					if (data.acl.ace.length == undefined){
						data.acl.ace = [data.acl.ace];
					}
					for(var i=0;i<data.acl.ace.length;++i) {
						var currentAcl = data.acl.ace[i];
						for (var j=0;j<userRoles.length;++j) {
							var currentRole = userRoles[j];
							if (currentRole==adminRole) {
								canRead = true;
								canWrite = false;
								canContribute = true;
								break;
							}
							else if (currentAcl.role==currentRole) {
								if (currentAcl.action=="read" && currentAcl.allow) {
									canRead = true;
								}
								else if (currentAcl.action=="write" && currentAcl.allow) {
									canWrite = true;
								}
								else if (currentAcl.action=="contribute" && currentAcl.allow) {
									canContribute = true;
								}
							}
						}
					}
				}
				else if (data) {
					canRead = true;

					// Enable write if admin
					for (var i=0;i<userRoles.length;++i) {
						var currentRole = userRoles[i];
						if (currentRole==adminRole) {
							canWrite = false;
							canContribute = true;
							break;
						}
					}
				}
				else {
					loadError = true;
				}

				paella.matterhorn.access.read = canRead;
				paella.matterhorn.access.write = canWrite;
				paella.matterhorn.access.contribute = canContribute;
				paella.matterhorn.access.isAnonymous = isAnonymous;
				onSuccess(canRead,canContribute,canWrite,loadError,isAnonymous);
			});
		});
	},

	loadUserRoles:function(onSuccess) {
		var params = {};
		var url = this.serverUrl + "info/me.json";
		new paella.Ajax(url,params,function(data) {
			if (typeof(data)=="string") {
				data = JSON.parse(data);
			}
			paella.matterhorn.me = data;
			onSuccess(data);
		},this.proxyUrl,this.useJsonp)
	},

	getPermissions:function(id,onSuccess) {
		var thisClass = this;
		var params = {"series":id, "episodes":"true", "limit":"1"};
		var url = this.serverUrl + "search/series.json";
		new paella.Ajax(url,params,function(data) {
			if (typeof(data)=='string') {
				data = JSON.parse(data);
			}
			var seriesId = data.dcIsPartOf;
			if (data["search-results"] && data["search-results"].total && parseInt(data["search-results"].total)>0) {
				paella.matterhorn.series.serie = data["search-results"].result;
				var seriesId = data["search-results"].result.dcIsPartOf;
				if (seriesId) {
					var aclParams = {};
					url = thisClass.serverUrl + "series/" + seriesId + "/acl.json";
					new paella.Ajax(url,aclParams,function(result) {
						if (typeof(result)=='string') {
							result = JSON.parse(result);
						}
						onSuccess(result);
					},thisClass.proxyUrl,thisClass.useJsonp);
				}
				else {
					var result = {acl:null}
					onSuccess(result);
				}
			}
			else {
				onSuccess(null);
			}
		},this.proxyUrl,this.useJsonp);
	}
});

paella.matterhorn.MatterhornData = Class.create({
	restServer:'',
	series:'',
	episodes:'',
	proxyUrl:false,
	useJsonp:false,

	initialize:function(config) {
		this.restServer = config.restServer.url;
		this.series = config.restServer.seriesJson;
		this.episodes = "search/episode.json";
		if (config.proxyLoader && config.proxyLoader.enabled) {
			this.proxyUrl = config.proxyLoader.url;
		}
		this.useJsonp = config.proxyLoader.usejsonp;
	},

	isStreaming:function(track) {
		return /rtmps*:\/\//.test(track.url) && track.mimetype && track.mimetype.match(/video/i);
	},

	loadVideoData:function(id,onSuccess) {
		var parameters = {id:id};
		var episodesUrl = this.restServer + this.episodes;
		var thisClass =this;

		new paella.Ajax(episodesUrl,{id:id},function(data) {
			var jsonData = data;
			if (typeof(jsonData)=="string") jsonData = JSON.parse(jsonData);

			var result = jsonData['search-results'].result;
			if (result) {
			    // Capture recording info and set page title
			    recordingTitle = result.dcTitle;
			    recordingStartDatetime = formatMediapackageDate(result.mediapackage.start);
			    seriesTitle = result.mediapackage.seriestitle;
			    document.title = recordingTitle + " | " + seriesTitle;
			    //
				paella.matterhorn.episode = result;
				var tracks = result.mediapackage.media.track;
				if (!(tracks instanceof Array)){
				    tracks = [tracks];
				}
				var presenterData = {sources:{}};
				var presentationData = {sources:{}};

				var presenterFound = false;
				var presentationFound = false;

				for (var i=0;i<tracks.length;++i) {
					var track = tracks[i];
					track.url = getCompliantURL(track.url);
					var sourceInfo = {}
					sourceInfo.src = track.url;
					sourceInfo.type = track.mimetype;
                    sourceInfo.duration = track.duration / 1000;
					var destinationData;

					if ((track.type=='presenter/delivery') || (track.type=='presentation/delivery')){
						if ((track.url != '') && track.mimetype && /video/i.test(track.mimetype)){
						    if (track.type=='presenter/delivery') {
								destinationData = presenterData;
								presenterFound = true;
						    }
						    else if (track.type=='presentation/delivery') {
								destinationData = presentationData;
								presentationFound = true;
						    }

						    if (thisClass.isStreaming(track)) {
						    	destinationData.sources.rtmp = sourceInfo;
						    }
						    else if (track.mimetype=='video/mp4') {
								destinationData.sources.mp4 = sourceInfo;
						    }
						    else if (track.mimetype=='video/ogg') {
								destinationData.sources.ogg = sourceInfo;
						    }
						    else if (track.mimetype=='video/webm') {
								destinationData.sources.webm = sourceInfo;
						    }
						    else if (track.mimetype=='video/x-flv') {
							    destinationData.sources.flv = sourceInfo;
						    }
						}
					}
				}

				var videoData = {};
				if (presenterFound && presentationFound) {
					videoData.master = presenterData;
					videoData.slave = presentationData;
				}
				else if (presenterFound) {
					videoData.master = presenterData;
					videoData.slave = null;
				}
				else if (presentationFound) {
					videoData.master = presentationData;
					videoData.slave = null;
				}

				videoData.frames = {};
				var unorderedFrames = {}
				var attachments = result.mediapackage.attachments.attachment;
				var lastFrameTime = 0;
				for (var attachment in attachments) {
					attachment = attachments[attachment];
					attachment.url = getCompliantURL(attachment.url);
					if (attachment.type=="presentation/segment+preview") {
						var url = attachment.url;
						var time = 0;
						if (/time=T(\d+):(\d+):(\d+)/.test(attachment.ref)) {
							time = parseInt(RegExp.$1)*60*60 + parseInt(RegExp.$2)*60 + parseInt(RegExp.$3);
						}
						unorderedFrames[time] = {time:time,url:url,mimetype:attachment.mimetype,id:attachment.id};
						if (time>lastFrameTime) lastFrameTime = time;
					}
					else if (attachment.type=="presentation/player+preview") {
						if (videoData.slave) { videoData.slave.preview = attachment.url;}
						else if (videoData.master) { videoData.master.preview = attachment.url;}
					}
					else if (attachment.type=="presenter/player+preview") {
						videoData.master.preview = attachment.url;
					}
				}

				for (var i=0;i<=lastFrameTime;++i) {
					if (unorderedFrames[i]) {
						videoData.frames[i] = unorderedFrames[i];
					}
				}
                onSuccess(videoData);
                $('#bannerTop').html(recordingTitle);
                $('#bannerTopDate').html(recordingStartDatetime + "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='https://ets.berkeley.edu/help/classroom-capture-video-player-beta-program' target='_blank'>Need help with this video player?</a>");
			}
			else {
				var message = paella.dictionary.translate("The specified video identifier does not exist");
				paella.messageBox.showError(message);
				$(document).trigger(paella.events.error,{error:message});
			}
		},this.proxyUrl,this.useJsonp);
	}
});

paella.matterhorn.AccessControl = Class.create(paella.AccessControl,{
	checkAccess:function(onSuccess) {
		var thisClass = this;
		var roleManager = new paella.matterhorn.UserRoleManager(paella.player.config);
		roleManager.checkAccess(function(canRead,canContribute,canWrite,loadError,isAnonymous) {
			thisClass.permissions.canRead = canRead;
			thisClass.permissions.canContribute = canContribute;
			thisClass.permissions.canWrite = canWrite;
			thisClass.permissions.loadError = loadError;
			thisClass.permissions.isAnonymous = isAnonymous;
			onSuccess(thisClass.permissions);
		});
	}
});

paella.matterhorn.VideoLoader = Class.create(paella.VideoLoader,{
	loadVideo:function(videoId,onSuccess) {
		var matterhornData = new paella.matterhorn.MatterhornData(paella.player.config);
		var thisClass = this;
		matterhornData.loadVideoData(videoId,function(videoData) {
			thisClass.loadStatus = true;
			thisClass.frameList = videoData.frames;
			thisClass.streams.push(videoData.master);
			if (videoData.slave) {
				thisClass.streams.push(videoData.slave);
			}
			onSuccess();
		});
	}
});


paella.matterhorn.AnnotationService = Class.create({
	serverUrl:'',
	proxyUrl:'',
	useJsonp:false,

	initialize:function(config) {
		this.serverUrl = config.restServer.url;
		if (config.proxyLoader && config.proxyLoader.enabled) {
			this.proxyUrl = config.proxyLoader.url;
		}
		this.useJsonp = config.proxyLoader.usejsonp;
	},

	getAnnotation:function(annotationId, onSuccess, onError) {
		var restEndpointGetAnnotation = this.serverUrl + "annotation/"+annotationId+".json";
		new paella.Ajax(restEndpointGetAnnotation, {}, function(response) {
			if (typeof(response)=="string") {
				try {
					response = JSON.parse(response);
				}
				catch(e) {response = null;}
			}
			if (response && response.annotation){
				if (onSuccess) onSuccess(response.annotation);
			}
			else{
				if (onError) onError();
			}
		}, this.proxyUrl, this.useJsonp, 'GET');
	},

	getAnnotations:function(episodeId, type, limit, offset, onSuccess, onError) {
		var restEndpointGetAnnotation = this.serverUrl + "annotation/annotations.json";
		new paella.Ajax(restEndpointGetAnnotation, {episode:episodeId, type:type, limit:limit, offset:offset}, function(response) {
			if (typeof(response)=="string") {
				try {
					response = JSON.parse(response);
				}
				catch(e) {response = null;}
			}
			if (response){
				if (onSuccess) onSuccess(response.annotations);
			}
			else{
				if (onError) onError();
			}
		}, this.proxyUrl, this.useJsonp, 'GET');
	},

	putAnnotation:function(episodeId, type, value, inPoint, outPoint, onSuccess, onError){
		var restEndpoint = this.serverUrl + "annotation";
		new paella.Ajax(restEndpoint,{episode:episodeid, type:type, in:inPoint, out:outPoint, value:value}, function(response) {
			if (onSuccess) onSuccess();
		}, this.proxyUrl, this.useJsonp, 'PUT');
	},

	updateAnnotation:function(annotationId, value, onSuccess, onError) {
		var restEndpoint = this.serverUrl + "annotation/" + annotationId;
		new paella.Ajax(restEndpoint, {value:value}, function(response) {
			if (onSuccess) onSuccess();
		}, this.proxyUrl, this.useJsonp, 'PUT');

	},

	deleteAnnotation:function(annotationId, onSuccess, onError) {
		var restEndpoint = this.serverUrl + "annotation/" + annotationId;
		new paella.Ajax(restEndpoint, {}, function(response) {
			if (onSuccess) onSuccess();
		}, this.proxyUrl, this.useJsonp, 'DELETE');
	}
});


paella.matterhorn.LoaderSaverInfo = Class.create({

	AsyncLoaderAjaxCallback: Class.create(paella.AsyncLoaderCallback,{
		url:null,
		params:null,
		onSuccess:null,
		proxyUrl:null,
		useJsonp:null,
		method:null,

		initialize:function(url,params,onSuccess,proxyUrl,useJsonp,method) {
			this.parent("pluginAjaxCallback");
			this.url = url;
			this.params = params;
			this.onSuccess = onSuccess;
			this.proxyUrl = proxyUrl;
			this.useJsonp = useJsonp;
			this.method = method;
		},

		load:function(onSuccess,onError) {
			var thisClass = this;
			new paella.Ajax(this.url,this.params, function(response) {
//						if (thisClass.onSuccess) thisClass.onSuccess(response);
						if (onSuccess) onSuccess();
			}, this.proxyUrl, this.useJsonp, this.method);
		}
	}),
	config:null,

	initialize:function(config) {
		this.config = config;
	},

	loadData:function(episodeid, type, onSuccess, onError){
		var proxyUrl = '';
		var useJsonp = this.config.proxyLoader.usejsonp;
		if (this.config.proxyLoader && this.config.proxyLoader.enabled) {
			proxyUrl = this.config.proxyLoader.url;
		}
		var restEndpointGetAnnotationsList = this.config.restServer.url + "annotation/annotations.json";
		new paella.Ajax(restEndpointGetAnnotationsList,{episode:episodeid, type:type, limit:1}, function(response) {
			if (typeof(response)=="string") {
				try {
					response = JSON.parse(response);
				}
				catch(e) {response = null;}
			}
			if (response){
				var value = "";
				if (response.annotations.total == 1){
					value = response.annotations.annotation.value;
					if (onSuccess) onSuccess(value);
				}
				else{
					if (onError) onError();
				}
			}
			else{
				if (onError) onError();
			}
		}, proxyUrl, useJsonp, 'GET');
	},

	saveData:function(episodeid, type, value, onSuccess, onError){
		var thisClass = this;
		var proxyUrl = '';
		var useJsonp = this.config.proxyLoader.usejsonp;
		if (this.config.proxyLoader && this.config.proxyLoader.enabled) {
			proxyUrl = this.config.proxyLoader.url;
		}
		var restEndpointGetAnnotationsList = this.config.restServer.url + "annotation/annotations.json";
		var restEndpointPutAnnotation = this.config.restServer.url + "annotation";
		var restEndpointDeleteAnnotation = this.config.restServer.url + "annotation";

		new paella.Ajax(restEndpointGetAnnotationsList,{episode:episodeid, type:type, limit:1000}, function(response) {
			if (typeof(response)=="string") {
				response = JSON.parse(response);
			}
			var asyncLoader = new paella.AsyncLoader();

			if (response.annotations.total>0) {
				if (!(response.annotations.annotation instanceof Array)){
				    response.annotations.annotation = [response.annotations.annotation];
				}
				//There are annotations of the desired type, deleting...
				for (var i=0; i< response.annotations.total; i=i+1){
					asyncLoader.addCallback(new thisClass.AsyncLoaderAjaxCallback(restEndpointDeleteAnnotation+"/"+response.annotations.annotation[i].annotationId, {}, function(response) {}, proxyUrl, useJsonp, 'DELETE'));
				}
			}

			asyncLoader.load(function() {
				new paella.Ajax(restEndpointPutAnnotation,{episode:episodeid, type:type, in:0, out:0, value:value}, function(response) {
					if (onSuccess) onSuccess();
				}, proxyUrl, useJsonp, 'PUT');
			},
			function() {
				if (onError) onError();
			});
		}, proxyUrl, useJsonp, 'GET');
	}

});


paella.matterhorn.SearchEpisode = Class.create({
	config:null,
	proxyUrl:'',
	recordingEntryID:'',
	useJsonp:false,
//	divLoading:null,
	divResults:null,

	AsyncLoaderPublishCallback: Class.create(paella.AsyncLoaderCallback,{
		config:null,
		recording:null,

		initialize:function(config, recording) {
			this.parent("AsyncLoaderPublishCallback");
			this.config = config;
			this.recording = recording;
		},

		load:function(onSuccess,onError) {
			var thisClass = this;

			var loader = new paella.matterhorn.LoaderSaverInfo(thisClass.config);

			loader.loadData(this.recording.id, "paella/publish", function(response) {

				if (response == true){
					thisClass.recording.entry_published_class = "published";
				}
				else if (response == false){
					thisClass.recording.entry_published_class = "unpublished";
				}
				else if (response == "undefined"){
					thisClass.recording.entry_published_class = "pendent";
				}

				onSuccess();
			}, function(){
				thisClass.recording.entry_published_class = "no_publish_info";
				onSuccess();
			});
		}
	}),

	initialize:function(config) {
		this.config = config;
		if (config.proxyLoader && config.proxyLoader.enabled) {
			this.proxyUrl = config.proxyLoader.url;
		}
		this.useJsonp = config.proxyLoader.usejsonp;
	},

	doSearch:function(params, domElement) {
		var thisClass = this;
		var divList = domElement;
		divList.innerHTML = "";
		this.recordingEntryID =	 domElement.id + "_entry_";

		// loading div
//		this.divLoading = document.createElement('div');
//		this.divLoading.id = thisClass.recordingEntryID + "_loading";
//		this.divLoading.className = "recordings_loading";
//		this.divLoading.innerHTML = paella.dictionary.translate("Searching...");
//		divList.appendChild(this.divLoading);

		// header div
		var divHeader = document.createElement('div');
		divHeader.id = thisClass.recordingEntryID + "_header";
		divHeader.className = "recordings_header";
		divList.appendChild(divHeader);

		this.divResults = document.createElement('div');
		this.divResults.id = thisClass.recordingEntryID + "_header_results";
		this.divResults.className = "recordings_header_results";
		divHeader.appendChild(this.divResults);

		var divNavigation = document.createElement('div');
		divNavigation.id = thisClass.recordingEntryID + "_header_navigation";
		divNavigation.className = "recordings_header_navigation";
		divHeader.appendChild(divNavigation);

		// loading results
		thisClass.setLoading(true);

		paella.debug.log("Params offet: " + params.offset);
		paella.debug.log("Params limit: " + params.limit);
		paella.debug.log("Params q: " + params.q);

	    var baseURL = thisClass.config.restServer.url;
        // Set page title
        $.getJSON(baseURL + "series/" + params.sid + ".json", function(data) {
            var seriesData = data['http://purl.org/dc/terms/'];
            var pageTitle = (seriesData && seriesData.title) ?  seriesData.title[0].value : "Recordings";
            document.title = pageTitle;
        });

		var resultsAvailable = true;
		var restEndpoint = baseURL + "search/paellaEpisodeListing.json";

		new paella.Ajax(restEndpoint, params, function(response) {
			if (typeof(response)=="string") {
				response = JSON.parse(response);
			}

			var resultsAvailable = (response !== undefined) &&
				(response['search-results'] !== undefined) &&
				(response['search-results'].total !== undefined);

			if (resultsAvailable === false) {
				paella.debug.log("Seach failed, respons:  " + response);
				return;
			}

			var totalItems = parseInt(response['search-results'].total);

			if (totalItems == 0) {
			    thisClass.setResults(paella.dictionary.translate("<p>No recordings available</p>"));
			} else {
				var offset = parseInt(response['search-results'].offset);
				var limit = parseInt(response['search-results'].limit);

				var startItem = offset;
				var endItem = offset + limit;
				if (startItem < endItem) {
				  startItem = startItem + 1;
				}

				// *******************************
				// UCB will show ALL recordings on one page so we only report TOTAL number of results.
				// if ((params.q === undefined) || (params.q == "")) {
				// 	thisClass.setResults(paella.dictionary.translate("Results {0}-{1} of {2}").replace(/\{0\}/g, startItem).replace(/\{1\}/g, endItem).replace(/\{2\}/g, totalItems));
				// } else {
				// 	thisClass.setResults(paella.dictionary.translate('Results {0}-{1} of {2} for "{3}"').replace(/\{0\}/g, startItem).replace(/\{1\}/g, endItem).replace(/\{2\}/g, totalItems).replace(/\{3\}/g, params.q));
				// }
				// *******************************
				var pluralize = (totalItems == 1) ? "" : "s";
    			thisClass.setResults(paella.dictionary.translate("{2} recording" + pluralize).replace(/\{2\}/g, totalItems));
				// *******************************

				// *******************************
				// *******************************
				// TODO
				var asyncLoader = new paella.AsyncLoader();
				var results = response['search-results'].result;
				if (results.dcCreated) {
				    // Single result in 'search-results' so we define the array
				    results = [results];
				}
				var restEndpointDeleteAnnotation = thisClass.config.restServer.url + "annotation/annotations.json";
				//There are annotations of the desired type, deleting...
				for (var i =0; i < results.length; ++i ){
					asyncLoader.addCallback(new thisClass.AsyncLoaderPublishCallback(thisClass.config, results[i]));
				}

				asyncLoader.load(function() {
					// create navigation div
					if (results.length < totalItems) {
						// current page
						var currentPage = 1;
						if (params.offset !== undefined) {
							currentPage = (params.offset / params.limit) + 1;
						}

						// max page
						var maxPage = parseInt(totalItems / params.limit);
						if (totalItems % 10 != 0) maxPage += 1;
						maxPage =  Math.max(1, maxPage);

						// previous link
						var divPrev = document.createElement('div');
						divPrev.id = thisClass.recordingEntryID + "_header_navigation_prev";
						divPrev.className = "recordings_header_navigation_prev";
						if (currentPage > 1) {
							var divPrevLink = document.createElement('a');
							divPrevLink.className = "recordings_header_navigation_link"
							divPrevLink.param_offset = (currentPage - 2) * params.limit;
							divPrevLink.param_limit	= params.limit;
							divPrevLink.param_q = params.q;
							$(divPrevLink).click(function(event) {
								var params = {};
								params.offset = this.param_offset;
								params.limit = this.param_limit;
								params.q = this.param_q;
								thisClass.doSearch(params, divList);
							});
							divPrevLink.innerHTML = paella.dictionary.translate("Previous");
							divPrev.appendChild(divPrevLink);
						} else {
							divPrev.innerHTML = paella.dictionary.translate("Previous");
						}
						divNavigation.appendChild(divPrev);

						var divPage = document.createElement('div');
						divPage.id = thisClass.recordingEntryID + "_header_navigation_page";
						divPage.className = "recordings_header_navigation_page";
						divPage.innerHTML = paella.dictionary.translate("Page:");
						divNavigation.appendChild(divPage);

						// take care for the page buttons
						var spanBeforeSet = false;
						var spanAfterSet = false;
						var offsetPages = 2;
						for (var i = 1; i <= maxPage; i++)	{
							var divPageId = document.createElement('div');
							divPageId.id = thisClass.recordingEntryID + "_header_navigation_pageid_"+i;
							divPageId.className = "recordings_header_navigation_pageid";

							if (!spanBeforeSet && currentPage >= 5 && i > 1 && (currentPage - (offsetPages + 2) != 1)) {
								divPageId.innerHTML = "..."
								i = currentPage - (offsetPages + 1);
								spanBeforeSet = true;
							}
							else if (!spanAfterSet && (i - offsetPages) > currentPage && maxPage - 1 > i && i > 4) {
								divPageId.innerHTML = "..."
								i = maxPage - 1;
								spanAfterSet = true;
							}
							else {
								if (i !== currentPage) {
									var divPageIdLink = document.createElement('a');
									divPageIdLink.className = "recordings_header_navigation_link"
									divPageIdLink.param_offset = (i -1) * params.limit;
									divPageIdLink.param_limit = params.limit;
									divPageIdLink.param_q = params.q;
									$(divPageIdLink).click(function(event) {
										var params = {};
										params.offset = this.param_offset;
										params.limit = this.param_limit;
										params.q = this.param_q;
										thisClass.doSearch(params, divList);
									});
									divPageIdLink.innerHTML = i
									divPageId.appendChild(divPageIdLink);
								} else {
									divPageId.innerHTML = i
								}
							}
							divNavigation.appendChild(divPageId);
						}

						// next link
						var divNext = document.createElement('div');
						divNext.id = thisClass.recordingEntryID + "_header_navigation_next";
						divNext.className = "recordings_header_navigation_next";
						if (currentPage < maxPage) {
							var divNextLink = document.createElement('a');
							divNextLink.className = "recordings_header_navigation_link"
							divNextLink.param_offset = currentPage * params.limit;
							divNextLink.param_limit	= params.limit;
							divNextLink.param_q = params.q;
							$(divNextLink).click(function(event) {
								var params = {};
								params.offset = this.param_offset;
								params.limit = this.param_limit;
								params.q = this.param_q;
								thisClass.doSearch(params, divList);
							});
							divNextLink.innerHTML = paella.dictionary.translate("Next");
							divNext.appendChild(divNextLink);
						} else {
							divNext.innerHTML = paella.dictionary.translate("Next");
						}
						divNavigation.appendChild(divNext);

					}

					// create recording divs
					for (var i =0; i < results.length; ++i ) {
						var recording = results[i];
						var divRecording = thisClass.createRecordingEntry(i, recording);
						divList.appendChild(divRecording);
					}
                    var audioElement = $('audio');
                    if (audioElement && audioElement.mediaelementplayer) {
                        audioElement.mediaelementplayer({
                            audioWidth: 600,
                            audioHeight: 30,
                            startVolume: 0.8,
                            loop: false,
                            enableAutosize: false,
                            features: ['playpause','current','progress','duration','volume'],
                            alwaysShowControls: false,
                            iPadUseNativeControls: true,
                            iPhoneUseNativeControls: true,
                            AndroidUseNativeControls: true,
                            enableKeyboard: true,
                            pauseOtherPlayers: true
                        });
                    }
                    imagePreview();
				}, null);
			}
			// finished loading
			thisClass.setLoading(false);
		}, this.proxyUrl, this.useJsonp);
	},

	setLoading:function(loading) {
//		if (loading == true) {
//			this.divLoading.style.display="block"
//		} else {
//			this.divLoading.style.display="none"
//		}
	},

	setResults:function(results) {
		//var divResults = document.getElementById(this.recordingEntryID + "_header_results");
		$('#recordingCount').html(results);
	},

	getUrlOfAttachmentWithType:function(recording, type) {
		for (var i =0; i < recording.mediapackage.attachments.attachment.length; ++i ){
			var attachment = recording.mediapackage.attachments.attachment[i];
			if (attachment.type === type) {
                return getCompliantURL(attachment.url);
			}
		}
		return "";
	},

    createRecordingEntry:function(index, recording) {
        var thisClass = this;
        var rootID = thisClass.recordingEntryID + index;

        var divEntry = document.createElement('div');
        divEntry.id = rootID;
        divEntry.setAttribute("role", "listitem");

        divEntry.className="recordings_entry " + recording.entry_published_class;
        if (index % 2 == 1) {
            divEntry.className=divEntry.className+" odd_entry";
        } else {
            divEntry.className=divEntry.className+" even_entry";
        }
        var previewUrl = thisClass.getUrlOfAttachmentWithType(recording, "presentation/player+preview");
        if ((previewUrl === undefined) || (previewUrl == "")) {
            previewUrl = thisClass.getUrlOfAttachmentWithType(recording, "presenter/player+preview");
        }
        var recordingEntry;
        if (previewUrl===undefined || previewUrl=="") {
            recordingEntry = this.getAudioOnlyListItem(divEntry, rootID, recording);
        } else {
            if (document.URL.toLowerCase().indexOf('https') > -1) {
                previewUrl = previewUrl.replace("http://","https://");
            }
            recordingEntry = this.getVideoListItem(divEntry, rootID, recording, previewUrl);
        }
        return recordingEntry;
    },

    getAudioOnlyListItem:function(div, rootID, recording) {
        var trackArray = recording.mediapackage.media.track;
        div.appendChild(this.getTitleDiv(recording, rootID, false));
        div.appendChild(this.getDateDiv(recording, rootID));
        var audioDiv = document.createElement('div');
        audioDiv.id = rootID + '_preview_container';
        audioDiv.className = 'recordings_entry_preview_container';
        var audio = document.createElement('audio');
        audio.id = rootID+'_audio';
        var track;
        for (var i=1, tot=trackArray.length; i < tot; i++) {
            if ((/^rtmp/).test(trackArray[i].url)) {
                track = trackArray[i];
                break;
            }
        }
        if (!track) {
            track = trackArray[0];
        }
        audio.src = track.url;
        $(audio).attr('type',track.mimetype);
        $(audio).attr('controls','controls');
        audioDiv.appendChild(audio);
        div.appendChild(audioDiv);
        return div;
    },

    getVideoListItem:function(div, rootID, recording, previewUrl) {
        var videoDiv = document.createElement('div');
        videoDiv.id = rootID+"_preview_container";
        videoDiv.className = "recordings_entry_preview_container";
        var anchor = document.createElement('a');
        anchor.id = rootID+"_preview_link";
        anchor.className = "preview";
        anchor.href = this.getPlayerURL(recording);
        $(anchor).attr("target", "_watch");
        var img = document.createElement('img');
        img.id = rootID+"_preview";
        img.src = previewUrl;
        img.className = "recordings_entry_preview";
        img.alt = "";
        anchor.appendChild(img);
        var hiddenSpan = document.createElement('span');
        hiddenSpan.className = "visuallyhidden";
        hiddenSpan.innerHTML = "opens in new window"
        anchor.appendChild(hiddenSpan);
        videoDiv.appendChild(anchor);
        div.appendChild(videoDiv);
        var textDiv = document.createElement('div');
        textDiv.id = rootID+"_text_container";
        textDiv.className = "recordings_entry_text_container";
        textDiv.appendChild(this.getTitleDiv(recording, rootID, true));
        textDiv.appendChild(this.getDateDiv(recording, rootID));
        div.appendChild(textDiv);
        return div;
    },

    getTitleDiv:function(recording, rootID, linkTitleToPlayer){
        var titleDiv = document.createElement('div');
        titleDiv.id = rootID+"_text_title_container";
        titleDiv.className = "recordings_entry_text_title_container";
        var titleText;
        if (linkTitleToPlayer){
            titleText = document.createElement('a');
            titleText.id = rootID+"_text_title";
            titleText.innerHTML = "<h2>" + recording.dcTitle + "</h2><span class='visuallyhidden'> - opens in new window</span>";
            titleText.className = "recordings_entry_text_title";
            titleText.href = this.getPlayerURL(recording);
            $(titleText).attr("target", "_watch");
        } else {
            titleText = document.createElement('h2');
            titleText.innerHTML = recording.dcTitle;
        }
        titleDiv.appendChild(titleText);
        return titleDiv;
    },

    getPlayerURL:function(recording, rootID) {
        return 'http://' + document.URL.split('/')[2] + '/paella/ui/watch.html?server='+paella.utils.parameters.get("server")+'&id=' + recording.id;
    },

    getDateDiv:function(recording, rootID) {
        var dateDiv = document.createElement('div');
        dateDiv.id = rootID+"_text_date";
        dateDiv.className = "recordings_entry_text_date";
        dateDiv.innerHTML = formatMediapackageDate(recording.mediapackage.start);
        return dateDiv;
    }
});
